﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class cameratest : MonoBehaviour {

    WebCamTexture frontWebCam;
    public RawImage raw;
    void Start()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        for (int i = 0; i < devices.Length; i++)
        {

            if(devices[i].isFrontFacing)
            {
                Debug.Log(devices[i].name);
                frontWebCam = new WebCamTexture(devices[i].name);
                break;
            }
        }
        raw.texture = frontWebCam;
        raw.material.mainTexture = frontWebCam;
        //plane.material.mainTexture = frontWebCam;
        //plane.material.shader = Shader.Find("UI/Default");
        frontWebCam.Play();

        
    }

}
