﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;

public class DeviceCameraController : MonoBehaviour
{
    public RawImage image;
    public RectTransform imageParent;
    public AspectRatioFitter imageFitter;

    // Device cameras
    WebCamDevice frontCameraDevice;
  //  WebCamDevice backCameraDevice;
    WebCamDevice activeCameraDevice;

    WebCamTexture frontCameraTexture;
  //  WebCamTexture backCameraTexture;
    WebCamTexture activeCameraTexture;

    // Image rotation
    Vector3 rotationVector = new Vector3(0f, 0f, 0f);

    // Image uvRect
    Rect defaultRect = new Rect(0f, 0f, 1f, 1f);
    Rect fixedRect = new Rect(0f, 1f, 1f, -1f);

    // Image Parent's scale
    Vector3 defaultScale = new Vector3(1f, 1f, 1f);
    Vector3 fixedScale = new Vector3(-1f, 1f, 1f);

    int cameranow = 0;

    void Start()
    {
        // Check for device cameras
        if (WebCamTexture.devices.Length == 0)
        {
            Debug.Log("No devices cameras found");
            return;
        }
        cameranow = WebCamTexture.devices.Length - 1;
        Debug.Log(cameranow);
        MainWnd.g_Self.m_Debug.text += "Camera count : " + WebCamTexture.devices.Length + "\n";
        foreach(WebCamDevice de in WebCamTexture.devices)
        {
            MainWnd.g_Self.m_Debug.text += de.name;
            Debug.Log(de.name);
        }
        // Get the device's cameras and create WebCamTextures with them
        frontCameraDevice = WebCamTexture.devices[cameranow];
        //backCameraDevice = WebCamTexture.devices.First();

        frontCameraTexture = new WebCamTexture(frontCameraDevice.name);
        //backCameraTexture = new WebCamTexture(backCameraDevice.name);

        // Set camera filter modes for a smoother looking image
        frontCameraTexture.filterMode = FilterMode.Trilinear;
       // backCameraTexture.filterMode = FilterMode.Trilinear;

        // Set the camera to use by default
        SetActiveCamera(frontCameraTexture);
    }

    public void ChangeNextCam()
    {
        cameranow--;
        if(cameranow<0)
        {
            cameranow = WebCamTexture.devices.Length - 1;
        }
        MainWnd.g_Self.m_Debug.text = "Using Camera " + WebCamTexture.devices[cameranow].name;
        frontCameraDevice = WebCamTexture.devices[cameranow];
        frontCameraTexture = new WebCamTexture(frontCameraDevice.name);
        SetActiveCamera(frontCameraTexture);
    
    }

    void OnDisable()
    {
        if (activeCameraTexture != null)
        {
            activeCameraTexture.Stop();
        }
    /*    frontCameraDevice = WebCamTexture.devices[cameranow];
        frontCameraTexture = new WebCamTexture(frontCameraDevice.name);
        SetActiveCamera(frontCameraTexture);*/
    }

    // Set the device camera to use and start it
    public void SetActiveCamera(WebCamTexture cameraToUse)
    {
        
        if (activeCameraTexture != null)
        {
            activeCameraTexture.Stop();
        }
        activeCameraTexture = cameraToUse;
        activeCameraDevice = WebCamTexture.devices.FirstOrDefault(device =>
            device.name == cameraToUse.deviceName);

       // texture.mainTexture = activeCameraTexture;
        //texture.material.mainTexture = activeCameraTexture;
        image.texture = activeCameraTexture;
        image.material.mainTexture = activeCameraTexture;

        activeCameraTexture.Play();
    }

    // Switch between the device's front and back camera
    public void SwitchCamera()
    {
     //   SetActiveCamera(activeCameraTexture.Equals(frontCameraTexture) ?
       //     backCameraTexture : frontCameraTexture);
    }

    // Make adjustments to image every frame to be safe, since Unity isn't 
    // guaranteed to report correct data as soon as device camera is started
    void Update()
    {
        //if no camera return 
        if (activeCameraTexture == null)
        {
            TipWnd.Create("No Support Camera Found", () =>
            {
                LoginWnd.Create();
                LoginWnd.g_Self.ObjShow(true);
            });
            TipWnd.g_Self.ObjShow(true);
            CameraWnd.g_Self.DestroySelf();
            return;
        }

        // Skip making adjustment for incorrect camera data
        if (activeCameraTexture.width < 100)
        {
           // Debug.Log("Still waiting another frame for correct info...");
            return;
        }

        // Rotate image to show correct orientation 
        rotationVector.z = -activeCameraTexture.videoRotationAngle;
        image.rectTransform.localEulerAngles = rotationVector;
       // texture.transform.localEulerAngles = rotationVector;
        // Set AspectRatioFitter's ratio
        float videoRatio =
            (float)activeCameraTexture.width / (float)activeCameraTexture.height;
        imageFitter.aspectRatio = videoRatio;
        //texture.aspectRatio = videoRatio;
       // texture.SetDimensions((int)(1280*videoRatio), 1280);
        

        // Unflip if vertically flipped
        image.uvRect =
            activeCameraTexture.videoVerticallyMirrored ? fixedRect : defaultRect;
      //  texture.uvRect =
      //      activeCameraTexture.videoVerticallyMirrored ? fixedRect : defaultRect;
        // Mirror front-facing camera's image horizontally to look more natural
        imageParent.localScale =
            activeCameraDevice.isFrontFacing ? fixedScale : defaultScale;
      //  texture.transform.localScale = activeCameraDevice.isFrontFacing ? fixedScale : defaultScale;
    }

    public void toggle()
    {
        if (image.enabled)
        {
            image.enabled = false;
        }
        else
        {
            image.enabled = true;
        }
    }
}