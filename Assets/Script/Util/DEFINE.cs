﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DEFINE
{
    public class DEFINEDATA
    {

    }

    public class URL
    {
        /// <summary>
        /// Server時間
        /// no para
        ///
        /// </summary>
        public const string TIME = "http://highscope.incku.com/api/mobile/getServerDateTime.php";
        /// <summary>
        /// 登入
        /// Method : Post
        /// Parameters : 
        /// id
        /// pass
        /// </summary>
        public const string LOGIN = "http://highscope.incku.com.tw/api/mobile/login.php";
        /// <summary>
        /// 步數
        /// Method : Post
        /// Parameters : 
        /// uid
        /// start_date
        /// end_date
        /// </summary>
        public const string STEP = "http://highscope.incku.com/api/mobile/get_user_step_by_date_ranger.php ";
        /// <summary>
        /// 卡洛里
        /// Method : Post
        /// Parameters : 
        /// uid
        /// start_date
        /// end_date
        /// </summary>
        public const string CALORIE = "http://highscope.incku.com.tw/api/mobile/get_user_calorie_by_date_ranger.php";
        /// <summary>
        /// 活動強度
        /// Method : Post
        /// Parameters : 
        /// uid
        /// start_date
        /// end_date
        /// </summary>
        public const string ACTIVE = "http://highscope.incku.com.tw/api/mobile/get_user_active_by_date_ranger.php";
        /// <summary>
        /// 睡眠
        /// Method : Post
        /// Parameters : 
        /// uid
        /// start_date
        /// end_date
        /// </summary>
        public const string SLEEP = "http://highscope.incku.com.tw/api/mobile/get_user_sleep_by_date_range.php";
        /// <summary>
        /// 距離
        /// Method : Post
        /// Parameters : 
        /// uid
        /// start_date
        /// end_date
        /// </summary>
        public const string DISTANCE = "http://highscope.incku.com.tw/api/mobile/get_distance_by_date_ranger.php";
        /// <summary>
        /// just for test, take any argument
        /// </summary>
        public const string TEST = "";

    }

    public class RequireField
    {
        public static WWWForm LOGIN(string ID, string Password)
        {
            WWWForm form = new WWWForm();
            form.AddField("id", ID.ToString());
            form.AddField("pass", Password);
            return form;
        }
        public static WWWForm SLEEP(long UID, ServerDate start_date, ServerDate end_date)
        {
            WWWForm form = new WWWForm();
            form.AddField("uid", UID.ToString());
            form.AddField("start_date", start_date.ToString());
            form.AddField("end_date", end_date.ToString());
            return form;
        }

        public static WWWForm CALORIEHOUR(long UID, ServerDate start_date)
        {
            WWWForm form = new WWWForm();
            form.AddField("uid", UID.ToString());
            form.AddField("start_date", start_date.ToString());
            form.AddField("end_date", start_date.ToString());
            return form;
        }

        public static WWWForm CALORIEDAYS(long UID, ServerDate start_date, ServerDate end_date)
        {
            if ((end_date.Date - start_date.Date).Days == 0)
            {
                end_date.Date = end_date.Date.AddDays(1f);
            }
            WWWForm form = new WWWForm();
            form.AddField("uid", UID.ToString());
            form.AddField("start_date", start_date.ToString());
            form.AddField("end_date", end_date.ToString());
            return form;
        }
        public static WWWForm STEPSHOUR(long UID, ServerDate start_date)
        {
            WWWForm form = new WWWForm();
            form.AddField("uid", UID.ToString());
            form.AddField("start_date", start_date.ToString());
            form.AddField("end_date", start_date.ToString());
            return form;
        }

        public static WWWForm STEPSDAYS(long UID, ServerDate start_date, ServerDate end_date)
        {
            if ((end_date.Date - start_date.Date).Days == 0)
            {
                end_date.Date = end_date.Date.AddDays(1f);
            }
            WWWForm form = new WWWForm();
            form.AddField("uid", UID.ToString());
            form.AddField("start_date", start_date.ToString());
            form.AddField("end_date", end_date.ToString());
            return form;
        }
        public static WWWForm ACTIVE(long UID, ServerDate start_date, ServerDate end_date)
        {
            WWWForm form = new WWWForm();
            form.AddField("uid", UID.ToString());
            form.AddField("start_date", start_date.ToString());
            form.AddField("end_date", end_date.ToString());
            return form;
        }
        public static WWWForm DISTANCE(long UID, ServerDate start_date, ServerDate end_date)
        {
            WWWForm form = new WWWForm();
            form.AddField("uid", UID.ToString());
            form.AddField("start_date", start_date.ToString());
            form.AddField("end_date", end_date.ToString());
            return form;
        }
    }

}