﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;


public static class Tools
{
    /// <summary>
    ///將 localPosition, localEulerAngles, localScale初始化
    /// </summary>
    /// <param name="aObj">A object.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
    public static void GameObjectIni<T>(this T aObj) where T : MonoBehaviour
    {
        GameObjectIni(aObj, Vector3.zero);
    }

    public static void GameObjectIni<T>(this T aObj, Vector3 vPos) where T : MonoBehaviour
    {
        GameObjectIni(aObj.gameObject, vPos);
    }

    public static void GameObjectIni(this GameObject aObj)
    {
        GameObjectIni(aObj, Vector3.zero);
    }

    public static void GameObjectIni(this GameObject aObj, Vector3 vPos)
    {
        aObj.transform.localPosition = vPos;
        aObj.transform.localEulerAngles = Vector3.zero;
        aObj.transform.localScale = Vector3.one;
    }

    public static void MergeDictionary<TKey, TValue>(this Dictionary<TKey, TValue> me, Dictionary<TKey, TValue> merge)
    {
        foreach (var item in merge)
        {
            me[item.Key] = item.Value;
        }
    }
    public static void SetLayerRecursively(this GameObject go, int layerNumber)
    {
        foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.layer = layerNumber;
            // Debug.Log(" set " + trans.name + " " + trans.gameObject.layer);
        }
    }

}