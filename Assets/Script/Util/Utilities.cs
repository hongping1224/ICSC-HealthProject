﻿using UnityEngine;
using System.Collections;
using System;
public class Utilities {

   public static bool LineIntersectionPoint(Vector3 ps1, Vector3 pe1, Vector3 ps2,Vector3 pe2, out Vector2 intersectpoint)
    {
        intersectpoint = Vector2.zero;
        // Get A,B,C of first line - points : ps1 to pe1
        float A1 = pe1.y - ps1.y;
        float B1 = ps1.x - pe1.x;
        float C1 = A1 * ps1.x + B1 * ps1.y;

        // Get A,B,C of second line - points : ps2 to pe2
        float A2 = pe2.y - ps2.y;
        float B2 = ps2.x - pe2.x;
        float C2 = A2 * ps2.x + B2 * ps2.y;

        // Get delta and check if the lines are parallel
        float delta = A1 * B2 - A2 * B1;
        if (delta == 0)
            return false;

        // now return the Vector2 intersection point
       intersectpoint  = new Vector2((B2 * C1 - B1 * C2) / delta,(A1 * C2 - A2 * C1) / delta);
       return true;
    }

    public static string ToTimeString(float f)
    {
        string s = Mathf.FloorToInt(f / 3600f).ToString();
        s += ":" + Mathf.FloorToInt((f % 3600f) / 60f).ToString("D2");
        s += ":" + Mathf.FloorToInt(f % 60f).ToString("D2");
        return s;
    }

    public static Guid lUnique
    {
        get
        {
          /*  lUniqueCnt++;
            if (((lCreateTick / 10) + lUniqueCnt) * 10 < Tick)
            {
                lCreateTick = Tick;
                lUniqueCnt = 0;
            }
            return ((lCreateTick / 10) + lUniqueCnt) * 10 + cSubSet;*/
            return Guid.NewGuid();
        }
    }
}
