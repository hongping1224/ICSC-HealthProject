﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
public static class XmlManager
{

    public static void Save<T>(T obj, string filelocation)
    {
        CreateXML(SerializeObject<T>(obj),filelocation);
    }

    public static T Load<T>(string filelocation) where T : SaveData<T> , new()
    {
        if (File.Exists(filelocation))
        {
            return DeserializeObject<T>(LoadXML(filelocation));
        }
        else
        {
            T tmp = new T();
            tmp.Save();
            return new T();
        }
    }

    private static string SerializeObject<T>(T pObject)
    {
        string XmlizedString = null;
        MemoryStream memoryStream = new MemoryStream();
        XmlSerializer xs = new XmlSerializer(typeof(T));
        XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
        xs.Serialize(xmlTextWriter, pObject);
        memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
        return XmlizedString;
    }
    private static T DeserializeObject<T>(string pXmlizedString)
    {
        XmlSerializer xs = new XmlSerializer(typeof(T));
        MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(pXmlizedString));
        // XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
        return (T)xs.Deserialize(memoryStream);
    }

    private static string UTF8ByteArrayToString(byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        string constructedString = encoding.GetString(characters);
        return (constructedString);
    }
    private static byte[] StringToUTF8ByteArray(string pXmlString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        byte[] byteArray = encoding.GetBytes(pXmlString);
        return byteArray;
    }
    private static void CreateXML(string data, string fileLocation)
    {
        StreamWriter writer;
        FileInfo t = new FileInfo(fileLocation);
        if (!t.Exists)
        {
            writer = t.CreateText();
        }
        else
        {
            t.Delete();
            writer = t.CreateText();
        }
        writer.Write(data);
        writer.Close();

    }

    private static string LoadXML(string fileLocation)
    {
        if (File.Exists(fileLocation))
        {

            StreamReader r = File.OpenText(fileLocation);
            string _info = r.ReadToEnd();
            r.Close();

            return _info;
        }
        //  Debug.Log("file not found");
        return "";
    }

}