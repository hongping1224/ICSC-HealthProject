﻿using DEFINE;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace GameDataReader
{

	public class XmlData
	{
		public static string XmlPath = ".\\setting/";
		public static Func<string, XmlTextReader> GetXmlTextReader;

		///基本要讀的表格都放這裡
		public static void ReadXmlData()
		{
			try
			{
				_UNIVERSITY_.Reader("university");
                _MAP_.Reader("Map");
                _RUNOBJECT_.Reader("ObjectList");
                _MINI_GAME_.Reader("MiniGameLevel");
                _PATH_.Reader("Path");
#if BRAC_SYMBOL
                _LEVEL_.Reader("blevel");
#else
                _LEVEL_.Reader("nlevel");
#endif
            }
			catch(Exception e)
			{
					UnityEngine.Debug.Log(e);
				//Console.WriteLine(e);
			}
		}

#if SERVER
		
		///只有在Server開啟的時候才檢查表格內容 只做初步檢查
		public static void CheckXmlData()
		{
		}

		#endif
	}

	public static class FuncClass
	{
		static public int GetInt(this Hashtable aTable, string name)
		{
			return GetInt(aTable, name, 0);
		}

		static public int GetInt(this Hashtable aTable, string name, int Default)
		{
			if(aTable == null || aTable.Contains(name) == false)
				return Default;
			else
			if(aTable[name].ToString().StartsWith("0x"))
				return Convert.ToInt32(aTable[name].ToString().Substring(2), 16);
			else
				return Convert.ToInt32(aTable[name]);
		}

		static public List<int> GetIntToList(this Hashtable aTable, string name, int nCount)
		{
			List<int> data = new List<int>();
			for(int i = 1; i <= nCount; i++)
			{
				data.Add(GetInt(aTable, name + i));
			}
			return data;
		}

		static public uint GetUInt(this Hashtable aTable, string name)
		{
			return GetUInt(aTable, name, 0);
		}

		static public uint GetUInt(this Hashtable aTable, string name, uint Default)
		{
			if(aTable == null || aTable.Contains(name) == false)
				return Default;
			else
			if(aTable[name].ToString().StartsWith("0x"))
				return Convert.ToUInt32(aTable[name].ToString().Substring(2), 16);
			else
				return Convert.ToUInt32(aTable[name]);
		}

		static public List<uint> GetUIntToList(this Hashtable aTable, string name, int nCount)
		{
			List<uint> data = new List<uint>();
			for(int i = 1; i <= nCount; i++)
			{
				data.Add(GetUInt(aTable, name + i));
			}
			return data;
		}

		static public long GetLong(this Hashtable aTable, string name)
		{
			return GetLong(aTable, name, 0);
		}

		static public long GetLong(this Hashtable aTable, string name, long Default)
		{
			if(aTable == null || aTable.Contains(name) == false)
				return Default;
			long n = Convert.ToInt64(aTable[name]);
			return n;
		}

		static public List<long> GetLongToList(this Hashtable aTable, string name, int nCount)
		{
			List<long> data = new List<long>();
			for(int i = 1; i <= nCount; i++)
			{
				data.Add(GetLong(aTable, name + i));
			}
			return data;
		}

		static public ulong GetULong(this Hashtable aTable, string name)
		{
			return GetULong(aTable, name, 0);
		}

		static public ulong GetULong(this Hashtable aTable, string name, ulong Default)
		{
			if(aTable == null || aTable.Contains(name) == false)
				return Default;
			ulong n = Convert.ToUInt64(aTable[name]);
			return n;
		}

		static public List<ulong> GetULongToList(this Hashtable aTable, string name, int nCount)
		{
			List<ulong> data = new List<ulong>();
			for(int i = 1; i <= nCount; i++)
			{
				data.Add(GetULong(aTable, name + i));
			}
			return data;
		}

		static public short GetShort(this Hashtable aTable, string name)
		{
			return GetShort(aTable, name, 0);
		}

		static public short GetShort(this Hashtable aTable, string name, short Default)
		{
			if(aTable != null && aTable.Contains(name))
				return Convert.ToInt16(aTable[name]);
			else
				return Default;
		}

		static public List<short> GetShortToList(this Hashtable aTable, string name, int nCount)
		{
			List<short> data = new List<short>();
			for(int i = 1; i <= nCount; i++)
			{
				data.Add(GetShort(aTable, name + i));
			}
			return data;
		}

		static public ushort GetUShort(this Hashtable aTable, string name)
		{
			return GetUShort(aTable, name, 0);
		}

		static public ushort GetUShort(this Hashtable aTable, string name, ushort Default)
		{
			if(aTable != null && aTable.Contains(name))
				return Convert.ToUInt16(aTable[name]);
			else
				return Default;
		}

		static public List<ushort> GetUShortToList(this Hashtable aTable, string name, int nCount)
		{
			List<ushort> data = new List<ushort>();
			for(int i = 1; i <= nCount; i++)
			{
				data.Add(GetUShort(aTable, name + i));
			}
			return data;
		}

		static public string GetString(this Hashtable aTable, string name)
		{
			return GetString(aTable, name, "");
		}

		static public string GetString(this Hashtable aTable, string name, string Default)
		{
			if(aTable != null && aTable.Contains(name))
				return Convert.ToString(aTable[name]);
			else
				return Default;
		}

		static public List<string> GetStringToList(this Hashtable aTable, string name, int nCount)
		{
			List<string> data = new List<string>();
			for(int i = 1; i <= nCount; i++)
			{
				data.Add(GetString(aTable, name + i));
			}
			return data;
		}

		static public float GetFloat(this Hashtable aTable, string name)
		{
			return GetFloat(aTable, name, 0.0f);
		}

		static public float GetFloat(this Hashtable aTable, string name, float Default)
		{
			if(aTable != null && aTable.Contains(name))
				return Convert.ToSingle(aTable[name]);
			else
				return Default;
		}

		static public List<float> GetFloatToList(this Hashtable aTable, string name, int nCount)
		{
			List<float> data = new List<float>();
			for(int i = 1; i <= nCount; i++)
			{
				data.Add(GetFloat(aTable, name + i));
			}
			return data;
		}

		static public byte GetByte(this Hashtable aTable, string name)
		{
			return GetByte(aTable, name, 0);
		}

		static public byte GetByte(this Hashtable aTable, string name, byte Default)
		{
			if(aTable != null && aTable.Contains(name))
				return Convert.ToByte(aTable[name]);
			else
				return Default;
		}

		static public List<byte> GetByteToList(this Hashtable aTable, string name, int nCount)
		{
			List<byte> data = new List<byte>();
			for(int i = 1; i <= nCount; i++)
			{
				data.Add(GetByte(aTable, name + i));
			}
			return data;
		}

		public static bool IsSpecialWord(string pText)
		{
			if(pText.Length > 0)
			{
				string SpecialWord = "~`!@#$%^&*()=+|?<>/,;'\"\\{} ";

				for(int i = 0; i < pText.Length; i++)
				{
					for(int j = 0; j < SpecialWord.Length; j++)
					{
						if(pText[i] == SpecialWord[j])
							return true;
					}
				}

				return false;
			}

			return true;
		}

		public static bool IsReservedWord(string pText)
		{
			string dataLine = null;

			for(uint i = 1;; i++)
			{
				if(_RESERVEDDATA_.GetData(i) == null)
					break;

				dataLine = _RESERVEDDATA_.GetString(i);

				if(pText.Contains(dataLine))
					return true;
			}

			return false;
		}

		public static bool IsErrorWord(string pText)
		{
			return IsSpecialWord(pText) || IsFilterWord(pText) || IsReservedWord(pText);
		}

		public static bool IsFilterWord(string pText)
		{
			string dataLine = null;

			for(uint i = 1;; i++)
			{
				if(_FILTERDATA_.GetData(i) == null)
					break;

				dataLine = _FILTERDATA_.GetString(i);

				if(pText.Contains(dataLine))
					return true;
			}

			return false;
		}
	}


	public class XmlDataGroup<T> : XmlData<T>  where T : XmlDataGroup<T>, new()
	{
		new public static Dictionary<uint,List<T>> m_Datas = new Dictionary<uint, List<T>>();

		new public static void ClearData()
		{
			m_Datas.Clear();
		}

		new public static void Reader(string pFileName)
		{
			Console.WriteLine("Read : {0}", pFileName);
			Reader(pFileName, KeyFunc, Read);
		}

		new protected static void Read()
		{
			if(m_Key != 0)
			{
				if(!m_Datas.ContainsKey(m_Key))
					m_Datas.Add(m_Key, new List<T>());
				m_Datas[m_Key].Add(new T());
			}
		}

		new public static List<T> GetData(uint uKey)
		{
			List<T> r = null;
			m_Datas.TryGetValue(uKey, out r);
			return r == null ? new List<T>() : r.ToList();
		}
	}

	public class XmlData<T> : XmlData<uint, T> where T : XmlData<T>, new()
	{
		public uint id = KeyFunc();

		public static uint KeyFunc()
		{
			return m_Data.GetUInt("id");
		}

		public static void Reader(string pFileName)
		{
			Console.WriteLine("Read : {0}", pFileName);
			Reader(pFileName, KeyFunc, Read);
		}

		protected static void Read()
		{
			if(m_Key != 0)
				m_Datas.Add(m_Key, new T());
		}
	}

	public class XmlData<K, T> : XmlData where T : XmlData<K, T>, new()
	{
		protected static Hashtable m_Data = null;
		public static Dictionary<K, T> m_Datas = new Dictionary<K, T>();
		protected static K m_Key;

		public static T GetData(K k)
		{
			T t = default(T);
			m_Datas.TryGetValue(k, out t);
			return t;
		}

		public static void ClearData()
		{
		}

		public static void Reader(string pFileName, Func<K> func, Action Read)
		{
#if SERVER
			XmlTextReader reader = GetXmlTextReader(XmlPath + pFileName + ".xml");
#else
			XmlTextReader reader = GetXmlTextReader(pFileName);
#endif
			ClearData();
			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element)
				{
					if(reader.LocalName == pFileName)
					{
						Hashtable info = new Hashtable();
						while(reader.MoveToNextAttribute())
						{
							info[reader.Name] = reader.GetAttribute(reader.Name);
						}
						m_Data = info;
						m_Key = func();
						Read();
					}
				}
			}
			reader.Close();
		}

		static public int GetInt(string name)
		{
			return GetInt(name, 0);
		}

		static public int GetInt(string name, int Default)
		{
			return m_Data.GetInt(name, Default);
		}

		static public List<int> GetIntToList(string name, int nCount)
		{
			return m_Data.GetIntToList(name, nCount);
		}

		static public uint GetUInt(string name)
		{
			return GetUInt(name, 0);
		}

		static public uint GetUInt(string name, uint Default)
		{
			return m_Data.GetUInt(name, Default);
		}

		static public List<uint> GetUIntToList(string name, int nCount)
		{
			return m_Data.GetUIntToList(name, nCount);
		}

		static public long GetLong(string name)
		{
			return GetLong(name, 0);
		}

		static public long GetLong(string name, long Default)
		{
			return m_Data.GetLong(name, Default);
		}

		static public List<long> GetLongToList(string name, int nCount)
		{
			return m_Data.GetLongToList(name, nCount);
		}

		static public ulong GetULong(string name)
		{
			return GetULong(name, 0);
		}

		static public ulong GetULong(string name, ulong Default)
		{
			return m_Data.GetULong(name, Default);
		}

		static public List<ulong> GetULongToList(string name, int nCount)
		{
			return m_Data.GetULongToList(name, nCount);
		}

		static public short GetShort(string name)
		{
			return GetShort(name, 0);
		}

		static public short GetShort(string name, short Default)
		{
			return m_Data.GetShort(name, Default);
		}

		static public List<short> GetShortToList(string name, int nCount)
		{
			return m_Data.GetShortToList(name, nCount);
		}

		static public ushort GetUShort(string name)
		{
			return GetUShort(name, 0);
		}

		static public ushort GetUShort(string name, ushort Default)
		{
			return m_Data.GetUShort(name, Default);
		}

		static public List<ushort> GetUShortToList(string name, int nCount)
		{
			return m_Data.GetUShortToList(name, nCount);
		}

		static public string GetString(string name)
		{
			return GetString(name, "");
		}

		static public string GetString(string name, string Default)
		{
			return m_Data.GetString(name, Default);
		}

		static public List<string> GetStringToList(string name, int nCount)
		{
			return m_Data.GetStringToList(name, nCount);
		}

		static public float GetFloat(string name)
		{
			return GetFloat(name, 0);
		}

		static public float GetFloat(string name, float Default)
		{
			return m_Data.GetFloat(name, Default);
		}

		static public List<float> GetFloatToList(string name, int nCount)
		{
			return m_Data.GetFloatToList(name, nCount);
		}

		static public byte GetByte(string name)
		{
			return GetByte(name, 0);
		}

		static public byte GetByte(string name, byte Default)
		{
			return m_Data.GetByte(name, Default);
		}

		static public List<byte> GetByteToList(string name, int nCount)
		{
			return m_Data.GetByteToList(name, nCount);
		}
	}

	////////////////////////////////////////// 以上基底 要改請三思 ///////////////////////////

	
	public class _FILTERDATA_ : XmlData<_FILTERDATA_>
	{
		static public string GetString(uint k)
		{
			_FILTERDATA_ v = GetData(k);
			if(v != null)
				return v.pString;
			else
				return "";
		}

		public readonly string pString = GetString("text");
	}

	
	public class _RESERVEDDATA_ : XmlData<_RESERVEDDATA_>
	{
		static public string GetString(uint k)
		{
			_RESERVEDDATA_ v = GetData(k);
			if(v != null)
				return v.pString;
			else
				return "";
		}

		public readonly string pString = GetString("text");
	}



	public class _UNIVERSITY_ : XmlDataGroup<_UNIVERSITY_>
	{
		public readonly string UniversityName = GetString("name");
		public readonly int LvLimit = GetInt("lv_limit");
		public readonly float HpValue = GetFloat("hp_value");
		public readonly uint LeaderSkillID = GetUInt("leader_skill");
		public readonly uint MemberSkillID = GetUInt("member_skill");
		/// <summary>
		/// 升級道具
		/// </summary>

		public readonly uint[] RankUpItem = (from tar in GetString("rank_up_item").Split('-')
		                                     where tar != string.Empty
		                                     let  XID = uint.Parse(tar)
		                                     select XID).ToArray();
        

		/// <summary>
		/// 升級道具數
		/// </summary>
		public readonly uint[] RankUpItemNum = (from tar in GetString("rank_up_item_num").Split('-')
		                                        where tar != string.Empty
		                                        let  XID = uint.Parse(tar)
		                                        select XID).ToArray();
		/// <summary>
		/// 稱號
		/// </summary>
		public readonly string Title = GetString("title");
		/// <summary>
		/// 所在地
		/// </summary>
		public readonly string Place = GetString("place");
		/// <summary>
		/// 武器
		/// </summary>
		public readonly string Weapon = GetString("weapon");
		/// <summary>
		/// 喜歡的東西
		/// </summary>
		public readonly string FavoriteThings = GetString("favorite_things");
		/// <summary>
		/// 幸運月
		/// </summary>
		public readonly string LuckyMonth = GetString("lucky_month");
		/// <summary>
		/// 說明
		/// </summary>
		public readonly string Comment = GetString("comment");
		/// <summary>
		/// 主畫面待機台詞
		/// </summary>
		public readonly List<string> MainWndLinesCol = new List<string>(4);
		/// <summary>
		/// 戰鬥台詞
		/// </summary>
		public readonly List<string> BattleLinesCol = new List<string>(3);
		public readonly string LoseLines = GetString("lose_lines");
		public readonly string TimeScore1 = GetString("time_score1");
		public readonly string TimeScore4 = GetString("time_score4");
		public readonly string DeadScore1 = GetString("dead_score1");
		public readonly string DeadScore4 = GetString("dead_score4");
		public readonly string HeadAtlas = GetString("HeadAtlas");

		public _UNIVERSITY_ ()
		{
			for(int i = 1; i <= 4; i++)
			{
				MainWndLinesCol.Add(GetString("mainwnd_lines" + i));
			}

			for(int i = 1; i <= 3; i++)
			{
				BattleLinesCol.Add(GetString("battle_lines" + i));
			}
		
		}

		public static _UNIVERSITY_ GetData(uint XID, int Lv)
		{
			if(Lv < 1)
				return null;
			var Datas = GetData(XID);
			return Datas.Count < Lv ? null : Datas[Lv - 1];
		}
	}

    public class _LEVEL_ : XmlData<_LEVEL_>
    {
        public readonly string Name = GetString("name");
        public readonly SNumber Salary = new SNumber(GetFloat("salary_value"), GetInt("salary_pow"));
        public readonly SNumber LV_Require = new SNumber(GetFloat("lv_value"), GetInt("lv_pow"));
        public readonly SNumber Click = new SNumber(GetFloat("click_value"), GetInt("click_pow"));
        public readonly float Lucky = GetFloat("lucky_click");
        public readonly float BadLuck = GetFloat("bad_click");
    }

    public class _MAP_ : XmlDataGroup<_MAP_>
    {
        public readonly uint LevelID = GetUInt("levelid");
        public readonly float X = GetFloat("x");
        public readonly float Y = GetFloat("y");
    }
    public class _RUNOBJECT_ : XmlData<_RUNOBJECT_>
    {
        public readonly int StartLength = GetInt("startLength");
        public readonly float Speed = GetFloat("speed");
        public readonly float Next_Delay = GetFloat("next_delay");
        public readonly ObjectBehave Behaveir = (ObjectBehave)GetInt("behave");
        public readonly string PrefebName = GetString("prefab_name");
        public readonly string DeathDes = GetString("death_des");
        public readonly string DeathSprite = GetString("death_pic");
        public readonly int Panelty = GetInt("lost_power");
    }

    public class _PATH_ : XmlDataGroup<_PATH_>
    {
        public readonly uint[] Objects = (from tar in GetString("object_id").Split('-')
                                          where tar != string.Empty
                                          let XID = uint.Parse(tar)
                                          select XID).ToArray();

        public readonly float NextPathDelay = GetFloat("separate");
    }

    public class _MINI_GAME_ : XmlDataGroup<_MINI_GAME_>
    {
        public readonly string LevelBG = GetString("level_bg");
        public readonly float SpeedUp = GetFloat("speedup");
        public readonly uint PathID = GetUInt("pathid");
        public readonly int Power = GetInt("power");
        public readonly float WinTime = GetFloat("time");
        public readonly int Reward = GetInt("reward");
    }

    public class _SFX_ : XmlData<_SFX_>
    {
        public readonly string Path = GetString("sfx_name");
    }

    public enum ObjectBehave
    {
        Normal =0,
    }

}