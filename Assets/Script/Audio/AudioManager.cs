﻿using UnityEngine;
using System.Collections;
using System;

public class AudioManager : GeneralWnd<AudioManager>
{
    private static float g_fBGMValue = 0.75f;
    private static float g_fSoundValue = 0.75f;

    public static AudioManager Create()
    {
        return Create(MainWnd.g_Self.transform, "ResGUI/Audio/AudioManager");
    }

    public void PlayBGM(string pName)
    {
        g_fBGMValue = PlayerPrefs.GetInt("bOpenBGM") == 0 ? 0.25f : 0f;
        LoadAudio("BGM", pName, PlayBGM);
    }

    public void PlaySound(string pName)
    {
        g_fSoundValue = PlayerPrefs.GetInt("bOpenSound") == 0 ? 0.75f : 0f;
        LoadAudio("SOUND", pName, PlaySound);
    }

    private void PlaySound(AudioClip ac)
    {
        audio.PlayOneShot(ac, g_fSoundValue);
        StartCoroutine(PlaySoundInCoroutine(ac));
    }

    private IEnumerator PlaySoundInCoroutine(AudioClip ac)
    {
        GameObject go = new GameObject();
        go.transform.parent = transform;
        var ad = go.AddComponent<AudioSource>();
        ad.clip = ac;
        ad.volume = g_fSoundValue;
        ad.Play();

        while (ad.isPlaying)
            yield return new WaitForEndOfFrame();
        GameObject.DestroyImmediate(go);
        yield break;
    }

    private void PlayBGM(AudioClip ac)
    {
        audio.volume = g_fBGMValue;
        audio.clip = ac;
        audio.loop = true;
        audio.Play();
    }

    public void LoadAudio(string pPtah, string pPrefabName, Action<AudioClip> callBack)
    {
        var Obj = Resources.Load(pPtah +"/" + pPrefabName) as AudioClip;
        if (Obj != null)
        {
            callBack(Obj);
            return;
        }
    }

}
