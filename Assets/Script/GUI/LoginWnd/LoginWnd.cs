﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DEFINE;
public class LoginWnd : GeneralWnd<LoginWnd>
{
    public UIInput m_UserNameLbl;
    public UIInput m_PasswordLbl;
    public event Action OnLoginSuccess;
    public GameObject m_SubmitButton;
    public UIToggle m_Toggle;
    private StatusNow m_statusNow = StatusNow.Start;
    private enum StatusNow 
    {
        Start,
        Logining,
        LoginDone,
        GettingAcvtive,
        ActiveDone,
        GettingStep,
        StepDone,
        GetCoin,
        Done,
    }


    public static LoginWnd Create()
    {
            return LoginWnd.Create(MainWnd.g_Self.transform, "ResGUI/LoginWnd/LoginWnd");
    }

    public void ToPassword()
    {
        UICamera.selectedObject = m_PasswordLbl.gameObject;
    }

    

    protected override void Ini()
    {
        base.Ini();
        m_UserNameLbl.value = GData.g_Setting.m_UserName;
        m_PasswordLbl.value = GData.g_Setting.m_Password;
        UIEventListener.Get(m_SubmitButton).onClick += (go) =>
            {
                Submit();
            };
        OnLoginSuccess += () =>
        {
            //save password
            yesterday = new ServerDate(GData.g_date.Date.AddDays(-1));
            if (m_Toggle.value)
            {
                GData.g_Setting.m_UserName = m_UserNameLbl.value;
                GData.g_Setting.m_Password = m_PasswordLbl.value;
                GData.g_Setting.Save();
            }
            else
            {
                SettingData.ClearFile();
            }
#if BRAC_SYMBOL
            // fetch yesterday data 
            m_statusNow = StatusNow.LoginDone;
#else
            m_statusNow = StatusNow.Done;
            ChangeWnd.LoadWnd<WorkWnd>(this);
#endif
        };
    }

    ServerDate yesterday = new ServerDate(GData.g_date.Date.AddDays(-1));
    protected override void Update()
    {
       // yesterday = new ServerDate("2016/09/02");
        base.Update();
        switch(m_statusNow)
        {
            case StatusNow.LoginDone:
                {
                   m_statusNow = StatusNow.GettingAcvtive;
                   ActiveConnection con2 = new ActiveConnection();
                   WWWForm form2 = DEFINE.RequireField.ACTIVE(GData.g_Player.m_User.m_UID, yesterday, yesterday);
                   con2.SentRequest(form2);
                   con2.OnSuccess += () =>
                       {
                           m_statusNow = StatusNow.ActiveDone;
                       };
                }
                break;
            case StatusNow.ActiveDone:
                {
                    m_statusNow = StatusNow.GettingStep;
                    StepsDayConnection con = new StepsDayConnection();
                    WWWForm form1 = DEFINE.RequireField.STEPSDAYS(GData.g_Player.m_User.m_UID, yesterday,yesterday);
                    con.SentRequest(form1);
                    con.OnSuccess += () =>
                    {
                        m_statusNow = StatusNow.StepDone;
                    };
                }
                break;
            case StatusNow.StepDone:
                {
                    m_statusNow = StatusNow.GetCoin;
                    ChangeWnd.LoadWnd<WorkWnd>(this);
                    TipWnd.Create(GData.GetCoinSpecial(yesterday));
                }
                break;
            case StatusNow.GetCoin:
                {
                    m_statusNow = StatusNow.Done;
                }
                break;
            case StatusNow.Done:
                {
                    m_isLoading = false;
                }
                break;
        }
    }

    bool m_isLoading = false;
    public void Submit()
    {
        if (!m_isLoading)
        {
            m_statusNow = StatusNow.Logining;
            ShortLoadingWnd.Create().ObjShow(true);
            LoginConnection lc = LoginConnection.Create();
            WWWForm form = RequireField.LOGIN(m_UserNameLbl.value, m_PasswordLbl.value);
            
            lc.OnSuccess += () =>
                {
                    //stop loading
                    ShortLoadingWnd.g_Self.DestroySelf();
                   // TipWnd.Create("登入成功");
                    if (OnLoginSuccess != null)
                        OnLoginSuccess();
                };
            lc.OnError += (string s) =>
                {
                    ShortLoadingWnd.g_Self.DestroySelf();
                    m_isLoading = false;
                    TipWnd.Create("密碼和賬號不符，請再登入一次");
                };

            lc.SentRequest(form);
            m_isLoading = true;
        }
    }

}
