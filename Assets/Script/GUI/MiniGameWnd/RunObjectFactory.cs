﻿using UnityEngine;
using System.Collections;
using GameDataReader;
public class RunObjectFactory
{
    public static RunningObj CreateObj(uint objid,Transform parent,Vector3 from ,Vector3 to,int road,float NextPathDelay)
    {
       _RUNOBJECT_ objject =  _RUNOBJECT_.GetData(objid);
       RunningObj t;
        if(objject == null)
        {
            return null;
        }
        switch(objject.id)
        {
            case 1:
                {
                    t = RHole.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 2:
                {
                    t = RPaper.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 3:
                {
                    t = RMan.Create(parent, from, to,true,road);
                    t.m_RunObject = objject;
                }
                break;
            case 4:
                {
                    if (road == 0)
                    {
                        t = RVase.Create(parent,false);
                        t.m_RunObject = objject;
                    }
                    else if (road == 2)
                    {
                        t = RVase.Create(parent, true);
                        t.m_RunObject = objject;
                    }
                    else
                    {
                        t = null;
                    }
                }
                break;
            case 5:
                {
                    t = RBikeSpawner.Create(parent, from, to,NextPathDelay -3f);
                    t.m_RunObject = objject;
                }
                break;
            case 6:
                {
                    t = RToolMan.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 7:
                {
                    t = RCar.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 8:
                {
                    RCar car = RCar.Create(parent, from, to);
                    t = car;
                    t.Behavior = (IBehaviour)new OpenDoorBehave(true, car);
                    t.m_RunObject = objject;
                }
                break;
            case 9:
                {
                    if (road == 0)
                    {
                        t = RUTurn.Create(parent, true);
                    }
                    else
                    {
                        t = RUTurn.Create(parent, false);
                    }
                    t.m_RunObject = objject;
                }
                break;
            case 10:
                {
                    t = RZombie.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 11:
                {
                    if (road == 0)
                    {
                        t = RCat.Create(parent, true);
                    }
                    else
                    {
                        t = RCat.Create(parent, false);
                    }
                    t.m_RunObject = objject;
                }
                break;
            case 12:
                {
                    t = RCouple.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 13:
                {
                    t = RFridge.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 14:
                {
                    t = RLittleGirl.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 15:
                {
                    t = RScammer.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 16:
                {
                    t = RTaiQiMaster.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 17:
                {
                    t = RTaiQi.Create(parent, from, to);
                    t.m_RunObject = objject;
                }
                break;
            case 18:
                {
                    t = RMan.Create(parent, from, to, true, road);
                    t.m_RunObject = objject;
                }
                break;
            case 19:
                {
                    RCar car = RCar.Create(parent, from, to);
                    t = car;
                    t.Behavior = (IBehaviour)new OpenDoorBehave(false,car);
                    t.m_RunObject = objject;
                }
                break;
            default :
                {
                    t = null;
                }
                break;
        }
        return t;
    }
}
