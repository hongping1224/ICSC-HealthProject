﻿using UnityEngine;
using System.Collections;

public class RUTurn : RunningObj
{
    public static RUTurn Create(Transform parent, bool flip)
    {
        RUTurn obj = CreateObj<RUTurn>(parent, "MiniGame/Running/UTurn", Vector3.zero);
        if(flip)
        {
            obj.m_Sprite.flip = UIBasicSprite.Flip.Horizontally;
        }
        obj.Behavior = new EmptyBehave();
        obj.m_SpriteAnimation.loop = false;
        return obj;
    }
    
    public override void UpdatePosition(float TimePast)
    {
       if(!m_SpriteAnimation.isPlaying)
       {
           m_IsDone = true;
       }
    }



}
