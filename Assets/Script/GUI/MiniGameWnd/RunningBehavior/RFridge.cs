﻿using UnityEngine;
using System.Collections;

public class RFridge : RunningObj {

    public static RFridge Create(Transform parent, Vector3 From, Vector3 To)
    {
        RFridge obj = CreateObj<RFridge>(parent, "MiniGame/Running/Fridge", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
}
