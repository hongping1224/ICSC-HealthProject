﻿using UnityEngine;
using System.Collections;

public class RScammer : RunningObj
{
    public static RScammer Create(Transform parent, Vector3 From, Vector3 To)
    {
        RScammer obj = CreateObj<RScammer>(parent, "MiniGame/Running/Scammer", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
}
