﻿using UnityEngine;
using System.Collections;

public class RCar : RunningObj
{
    public static RCar Create(Transform parent, Vector3 From, Vector3 To)
    {
        RCar obj = CreateObj<RCar>(parent, "MiniGame/Running/Car", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
    public override void UpdatePosition(float TimePast)
    {
        base.UpdatePosition(TimePast);
        if(transform.localPosition.y <=-200)
        {
            //open door
            Behavior.Behave();
        }
    }

}

public class OpenDoorBehave : IBehaviour
{
    bool triggerOnce = false;
    bool IsRight;
    RCar m_object;
    public OpenDoorBehave(bool Right,RCar obj)
    {
        IsRight = Right;
        m_object = obj;
    }
   
    public bool Condition()
    {
        return false;
    }
    public void Behave()
    {
        if (triggerOnce == false)
        {
            triggerOnce = true;
            if (IsRight)
            {
                //open right door
                m_object.m_Sprite.pivot = UIWidget.Pivot.Left;
                m_object.m_SpriteAnimation.namePrefix = "l";
                m_object.m_SpriteAnimation.loop = false;
                m_object.m_SpriteAnimation.ResetToBeginning();
                m_object.m_SpriteAnimation.Play();
                BoxCollider co = (BoxCollider)(m_object.m_Colider);
                co.center = new Vector3( -m_object.m_Sprite.transform.localPosition.x+150,0,0);
                co.size = new Vector3(300, co.size.y, co.size.z);
            }
            else
            {
                m_object.m_Sprite.pivot = UIWidget.Pivot.Right;
                m_object.m_SpriteAnimation.namePrefix = "r";
                m_object.m_SpriteAnimation.loop = false;
                m_object.m_SpriteAnimation.ResetToBeginning();
                m_object.m_SpriteAnimation.Play();
                BoxCollider co = (BoxCollider)(m_object.m_Colider);
                co.center = new Vector3(-m_object.m_Sprite.transform.localPosition.x - 150, 0, 0);
                co.size = new Vector3(300, co.size.y, co.size.z);
                //open leftdoor
                Debug.Log("left");
            }
        }
    }
   
}