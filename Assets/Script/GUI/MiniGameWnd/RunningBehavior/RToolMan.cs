﻿using UnityEngine;
using System.Collections;

public class RToolMan : RunningObj
{
    public static RToolMan Create(Transform parent, Vector3 From, Vector3 To)
    {
        RToolMan obj = CreateObj<RToolMan>(parent, "MiniGame/Running/ToolMan", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
}
