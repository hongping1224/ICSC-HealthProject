﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataReader;
public class RunningObj : GeneralObj
{
    public UISprite m_Sprite;
    public Collider m_Colider;
    public Vector3 m_Direction;
    public Vector3 From;
    public Vector3 To;
    public _RUNOBJECT_ m_RunObject;
    public IBehaviour Behavior;
    public UISpriteAnimation m_SpriteAnimation;
    public float m_Magnitute;
    public static readonly Vector3 StartingScale = new Vector3(0.33f, 0.33f, 0.33f);
    public bool m_IsDone = false;
    public RunningObjState m_State = RunningObjState.start;
    public float m_SpeedModifier = 1f;

    public virtual void UpdatePosition(float TimePast)
    {
        Vector3 moving = new Vector3((m_Direction.normalized * m_RunObject.Speed * TimePast*m_SpeedModifier).x, -(m_RunObject.Speed * TimePast*m_SpeedModifier),0);
        transform.localPosition += moving;
       
        float percent = (From.y - transform.localPosition.y)/ m_Magnitute;
        transform.localScale  = ( percent*(Vector3.one - StartingScale)) + StartingScale;
        if(percent >1)
        {
            m_IsDone = true;
        }
        if(m_State == RunningObjState.start)
        {
          if(m_Sprite.spriteName == "s"+m_RunObject.StartLength.ToString("D2"))
           {
               m_State = RunningObjState.move;
               m_SpriteAnimation.namePrefix = "m";
               m_SpriteAnimation.ResetToBeginning();
               m_SpriteAnimation.loop = true;
               m_SpriteAnimation.Play();
           }
        }
    }

    public virtual void SpecialBehave()
    {

    }
    protected override void Ini()
    {
        base.Ini();
        m_Colider.gameObject.tag = "RunningObj";
        m_SpriteAnimation.namePrefix = "s";
        m_SpriteAnimation.loop = false;
        m_SpriteAnimation.Play();
        transform.localScale = StartingScale;
    }

    public enum RunningObjState
    {
        start =0,
        move=1,
        end =2,
    }

}
