﻿using UnityEngine;
using System.Collections;
public class RTaiQiMaster : RunningObj
{
    public static RTaiQiMaster Create(Transform parent, Vector3 From, Vector3 To)
    {
        RTaiQiMaster obj = CreateObj<RTaiQiMaster>(parent, "MiniGame/Running/TaiQiMaster", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
    float timer = 0f;
    public override void UpdatePosition(float TimePast)
    {
        base.UpdatePosition(TimePast);
        timer += TimePast;
        if(timer >= m_RunObject.Next_Delay)
        {
            timer -= m_RunObject.Next_Delay;
            RunningObj t = RunObjectFactory.CreateObj(17,RunningObjManager.g_self.transform,transform.localPosition,To,0,0);
            RunningObjManager.g_self.m_WaitinToGenerate.Add(t);
        }
        
    }
}
