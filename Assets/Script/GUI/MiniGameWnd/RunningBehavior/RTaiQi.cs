﻿using UnityEngine;
using System.Collections;

public class RTaiQi : RunningObj {

    public static RTaiQi Create(Transform parent, Vector3 From, Vector3 To)
    {
        RTaiQi obj = CreateObj<RTaiQi>(parent, "MiniGame/Running/TaiQi", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
}
