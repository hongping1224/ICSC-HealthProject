﻿using UnityEngine;
using System.Collections;

public interface IBehaviour
{
    bool Condition();
    void Behave();
}

public class EmptyBehave :IBehaviour
{
    public bool Condition()
    {
        return false;
    }

    public void Behave()
    {

    }
}
