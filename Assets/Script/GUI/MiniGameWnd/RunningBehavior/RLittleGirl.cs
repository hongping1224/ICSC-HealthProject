﻿using UnityEngine;
using System.Collections;

public class RLittleGirl : RunningObj
{
    public static RLittleGirl Create(Transform parent, Vector3 From, Vector3 To)
    {
        RLittleGirl obj = CreateObj<RLittleGirl>(parent, "MiniGame/Running/LittleGirl", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
}
