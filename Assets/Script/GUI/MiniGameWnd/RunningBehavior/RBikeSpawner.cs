﻿using UnityEngine;
using System.Collections;

public class RBikeSpawner : RunningObj
{

    public static RBikeSpawner Create(Transform parent, Vector3 From, Vector3 To, float lifespan)
    {
        RBikeSpawner obj = CreateObj<RBikeSpawner>(parent, "MiniGame/Running/BikeSpawner", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        obj.LifeSpan = lifespan;
        return obj;
    }

    protected override void Ini()
    {
        transform.localScale = StartingScale;
    }

    float Timer = 0;
    float LifeSpan;
    public override void UpdatePosition(float TimePast)
    {
        Timer += TimePast;
        if(Timer >= m_RunObject.Next_Delay)
        {
            Timer -= 0.5f;
            RBike bike = RBike.Create(RunningObjManager.g_self.transform, From, To);
            bike.m_RunObject = m_RunObject;
            RunningObjManager.g_self.m_WaitinToGenerate.Add(bike);
            //generate bike;
        }
        LifeSpan -= TimePast;
        if(LifeSpan <=0)
        {
            DestroySelf();
        }
    }

}
