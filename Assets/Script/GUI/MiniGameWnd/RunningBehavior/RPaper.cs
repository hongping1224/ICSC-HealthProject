﻿using UnityEngine;
using System.Collections;
using GameDataReader;
public class RPaper : RunningObj
{
    public static RPaper Create(Transform parent, Vector3 From, Vector3 To)
    {
        RPaper obj = CreateObj<RPaper>(parent, "MiniGame/Running/Paper", From);
        obj.Behavior = new RPaperBehave(obj);
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
    public override void SpecialBehave()
    {
        base.SpecialBehave();
        if(m_State == RunningObjState.end)
        {
            if(!m_SpriteAnimation.isPlaying)
            {
                DestroySelf();
            }
        }
    }
}

public class RPaperBehave : IBehaviour
{
    RPaper m_objct;

    public RPaperBehave(RPaper obj)
    {
        m_objct = obj;
    }

    bool triggerOnce = false;

    public bool Condition()
    {
        if (triggerOnce == false)
        {
            if (m_objct.transform.localPosition.y <= -100)
            {
                triggerOnce = true;
                return true;
            }
        }
        return false;
    }

    public void Behave()
    {
        m_objct.m_SpriteAnimation.namePrefix = "e";
        m_objct.m_SpriteAnimation.ResetToBeginning();
        m_objct.m_SpriteAnimation.loop = false;
        m_objct.m_SpriteAnimation.Play(); 
        m_objct.m_Colider.enabled = false;
        m_objct.m_State = RunningObj.RunningObjState.end;
    }
  
}
