﻿using UnityEngine;
using System.Collections;
using GameDataReader;
public class RHole : RunningObj
{
    public static RHole Create(Transform parent, Vector3 From, Vector3 To)
    {
        RHole obj = CreateObj<RHole>(parent, "MiniGame/Running/Hole", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
}
