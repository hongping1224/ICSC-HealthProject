﻿using UnityEngine;
using System.Collections;

public class RBike : RunningObj
{
    public static RBike Create(Transform parent, Vector3 From, Vector3 To)
    {
        RBike obj = CreateObj<RBike>(parent, "MiniGame/Running/Bike", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
}
