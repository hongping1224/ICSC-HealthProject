﻿using UnityEngine;
using System.Collections;

public class RZombie : RunningObj
{
    public static RZombie Create(Transform parent, Vector3 From, Vector3 To)
    {
        RZombie obj = CreateObj<RZombie>(parent, "MiniGame/Running/Zombie", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }

    public override void SpecialBehave()
    {
        base.SpecialBehave();
        if (transform.localPosition.y <= -200)
            m_SpeedModifier = 2.0f;
    }

}



