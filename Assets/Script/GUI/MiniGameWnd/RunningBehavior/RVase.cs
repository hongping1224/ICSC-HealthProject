﻿using UnityEngine;
using System.Collections;

public class RVase : RunningObj {

    public static readonly Vector3 StartPos = new Vector3(-150, 268, 0);
    public static readonly Vector3 EndPos = new Vector3(-738, -860, 0);
    public static RVase Create(Transform parent,bool IsRight)
    {
        Vector3 From = StartPos;
        Vector3 To = EndPos;
        if(IsRight == false)
        {
            From = new Vector3(-StartPos.x, StartPos.y, StartPos.z);
            To = new Vector3(-EndPos.x, EndPos.y, EndPos.z);
        }
        RVase obj = CreateObj<RVase>(parent, "MiniGame/Running/Vase", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        if(IsRight)
        {
            obj.m_Sprite.pivot = UIWidget.Pivot.BottomLeft;
        }
        else
        {
            obj.m_Sprite.pivot = UIWidget.Pivot.BottomRight;
            obj.m_Sprite.flip = UIBasicSprite.Flip.Horizontally;
        }
        obj.m_Sprite.transform.localPosition = Vector3.zero;
        return obj;
    }

    public override void UpdatePosition(float TimePast)
    {
        Vector3 moving = new Vector3((m_Direction.normalized * m_RunObject.Speed * TimePast * m_SpeedModifier).x, -(m_RunObject.Speed * TimePast * m_SpeedModifier), 0);
        transform.localPosition += moving;
        float percent = (From.y - transform.localPosition.y) / m_Magnitute;
        transform.localScale = (percent * (Vector3.one - StartingScale)) + StartingScale;
        if (percent > 1)
        {
            m_IsDone = true;
        }
        if (m_State == RunningObjState.start)
        {
            m_State = RunningObjState.move;
        }
        if(fireOnce)
        {
            if(m_SpriteAnimation.isPlaying == false)
            {
                m_SpriteAnimation.namePrefix = "e";
                m_SpriteAnimation.loop = true;
                m_SpriteAnimation.ResetToBeginning();
                m_SpriteAnimation.Play();
            }
        }
    }
    bool fireOnce = false;
    public override void SpecialBehave()
    {
        base.SpecialBehave();
        if (transform.localPosition.y <= -200)
        {
            if (fireOnce == false)
            {
                fireOnce = true;
                m_SpriteAnimation.loop = false;
                m_SpriteAnimation.namePrefix = "m";
                m_SpriteAnimation.ResetToBeginning();
                m_SpriteAnimation.Play();
            }
        }
    }

}
