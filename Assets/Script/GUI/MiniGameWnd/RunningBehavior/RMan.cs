﻿using UnityEngine;
using System.Collections;

public class RMan : RunningObj
{
    bool m_IsGoRight = false;
    private int roadnow;
    public Vector3 tmpTo;
    public static RMan Create(Transform parent, Vector3 From, Vector3 To,bool GoRight,int road)
    {
        RMan obj = CreateObj<RMan>(parent, "MiniGame/Running/Man", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        obj.m_IsGoRight = GoRight;
        obj.roadnow = road;
        float midpoint = From.y -(Mathf.Abs(From.y-To.y)/2f);
        Vector2 intersect;
       Vector3 sidefrom= Vector3.zero;
       Vector3 sideto = Vector3.zero;
       if (obj.m_IsGoRight)
       {
           if (obj.roadnow < 2)
           {
               sidefrom = RunningObjManager.g_self.m_SpawnPoints[road + 1].localPosition;
               sideto = RunningObjManager.g_self.m_TargetPoints[road + 1].localPosition;
           }
       }
       else
       {
           if (obj.roadnow > 0)
           {
               sidefrom = RunningObjManager.g_self.m_SpawnPoints[road - 1].localPosition;
               sideto = RunningObjManager.g_self.m_TargetPoints[road - 1].localPosition;
           }
       }
        Utilities.LineIntersectionPoint(sidefrom, sideto, new Vector3(-2000, midpoint+200), new Vector3(2000, midpoint+200), out intersect);
        obj.tmpTo = intersect;
        return obj;
    }
    public override void SpecialBehave()
    {
        base.SpecialBehave();
    }
    public override void UpdatePosition(float TimePast)
    {
      
        float percent = (From.y - transform.localPosition.y) / m_Magnitute;
        transform.localScale = (percent * (Vector3.one - StartingScale)) + StartingScale;
        if (percent > 1)
        {
            m_IsDone = true;
        }
        if (transform.localPosition.y <= tmpTo.y)
        {
            tmpTo = To;
        }
        m_Direction = tmpTo - transform.localPosition;
        Vector3 moving = new Vector3((m_Direction.normalized*m_RunObject.Speed*TimePast).x, -(m_RunObject.Speed * TimePast), 0);
        transform.localPosition += moving;
       
        if (m_State == RunningObjState.start)
        {
            if (m_Sprite.spriteName == "s" + m_RunObject.StartLength.ToString("D2"))
            {
                m_State = RunningObjState.move;
                m_SpriteAnimation.namePrefix = "m";
                m_SpriteAnimation.ResetToBeginning();
                m_SpriteAnimation.loop = true;
                m_SpriteAnimation.Play();
            }
        }
    }
   
}




