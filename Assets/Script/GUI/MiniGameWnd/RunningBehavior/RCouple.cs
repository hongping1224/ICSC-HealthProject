﻿using UnityEngine;
using System.Collections;

public class RCouple : RunningObj
{
    public UISprite m_Shine;
    public static RCouple Create(Transform parent, Vector3 From, Vector3 To)
    {
        RCouple obj = CreateObj<RCouple>(parent, "MiniGame/Running/Couple", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = obj.m_Direction.magnitude;
        return obj;
    }
}
