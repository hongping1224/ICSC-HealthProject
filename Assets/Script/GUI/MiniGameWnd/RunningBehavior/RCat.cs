﻿using UnityEngine;
using System.Collections;

public class RCat : RunningObj
{
    public static readonly Vector3 StartPos = new Vector3(-259, 126,0);
    public static readonly Vector3 EndPos = new Vector3(649, -620, 0);
    public static RCat Create(Transform parent,bool IsRight)
    {
        
        Vector3 From = StartPos;
        Vector3 To = EndPos;
        if(IsRight == false)
        {
            From = new Vector3(-StartPos.x, StartPos.y, StartPos.z);
            To = new Vector3(-EndPos.x, EndPos.y, EndPos.z);
        }
      
        RCat obj = CreateObj<RCat>(parent, "MiniGame/Running/Cat", From);
        obj.Behavior = new EmptyBehave();
        obj.From = From;
        obj.To = To;
        obj.m_Direction = To - From;
        obj.m_Magnitute = Mathf.Abs( obj.m_Direction.magnitude*obj.m_Direction.normalized.y);
        if(IsRight == false)
        {
            obj.m_Sprite.flip = UIBasicSprite.Flip.Horizontally;
        }
        return obj;
    }
}


