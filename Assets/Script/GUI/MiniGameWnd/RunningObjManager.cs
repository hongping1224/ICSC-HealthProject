﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataReader;
public class RunningObjManager : MonoBehaviour
{
    public static RunningObjManager g_self;
    public List<Transform> m_SpawnPoints;
    public List<Transform> m_TargetPoints;
    public List<RunningObj> m_RunObj = new List<RunningObj>();
    public List<RunningObj> m_WaitingToDestroy = new List<RunningObj>();
    public List<RunningObj> m_WaitinToGenerate = new List<RunningObj>();
    public void Awake()
    {
        g_self = this;
    }

    public void Run(float TimePast)
    {
        while (m_RunObj.Contains(null))
        {
            m_RunObj.Remove(null);
        }
       foreach(RunningObj ro in m_WaitinToGenerate)
       {
           m_RunObj.Add(ro);
           ro.ObjShow(true);
       }
       m_WaitinToGenerate.Clear();
        foreach (RunningObj ro in m_RunObj)
        {
            ro.SpecialBehave();
            if (ro.m_State != RunningObj.RunningObjState.end)
            {
                ro.UpdatePosition(TimePast);
            }
            if (ro.Behavior.Condition())
            {
                ro.m_State = RunningObj.RunningObjState.end;
                ro.Behavior.Behave();
            }
            if (ro.m_IsDone)
            {
                m_WaitingToDestroy.Add(ro);
            }

        }
        foreach (RunningObj ro in m_WaitingToDestroy)
        {
           ro.DestroySelf();
        }
        m_WaitingToDestroy.Clear();

    }

    public void Spawn(_PATH_ path)
    {
        uint[] spawn = path.Objects;
        for (int i = 0; i < 3; i++)
        {
            RunningObj ob = RunObjectFactory.CreateObj(spawn[i], transform, m_SpawnPoints[i].localPosition, m_TargetPoints[i].localPosition,i,path.NextPathDelay);
            if (ob != null)
            {
                ob.ObjShow(true);
                m_RunObj.Add(ob);
            }
        }
    }
  
    public void UnRegisterObj(RunningObj obj)
    {
        m_RunObj.Remove(obj);
        obj.DestroySelf();
    }

}
