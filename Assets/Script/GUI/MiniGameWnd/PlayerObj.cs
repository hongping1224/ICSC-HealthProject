﻿using UnityEngine;
using System.Collections;

public class PlayerObj : GeneralObj
{
    public static PlayerObj Create(Transform parent, string atlasname,Vector3 pos)
    {
        PlayerObj po = CreateObj<PlayerObj>(parent, "ResGUI/MiniGameWnd/PlayerObj",pos);
        po.m_Sprite.atlas = Resources.Load<GameObject>("MiniGame/Player/" + atlasname).GetComponent<UIAtlas>();
        po.m_Animation.namePrefix = "m";
       return po;
    }

    public UISprite m_Sprite;
    public UISpriteAnimation m_Animation;

    protected override void Ini()
    {
        base.Ini();
    }
    

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "RunningObj")
        {
            Debug.Log("GetHit");
            MiniGameWnd.g_Self.m_IsPause = true;
            MiniGameWnd.g_Self.m_IsLose = true;
            RunningObj obj = col.GetComponent<RunningObj>();
            if (obj == null)
            {
                obj = col.GetComponentInParent<RunningObj>();
            }
            TipWnd.Create(obj.m_RunObject.DeathDes, () =>
            {
               SNumber S = (obj.m_RunObject.Panelty) * GData.g_Inventory.m_LevelNow.Salary;
               if(! GData.g_Inventory.TrySpendMoney(S))
               {
                   GData.g_Inventory.ClearMoney();
               }
                ChangeWnd.LoadWnd<WorkWnd>(MiniGameWnd.g_Self);
            });
        }
    }
}
