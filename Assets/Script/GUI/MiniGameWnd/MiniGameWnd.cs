﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using GameDataReader;
public class MiniGameWnd : GeneralWnd<MiniGameWnd>
{
    public static MiniGameWnd Create(uint level)
    {
        MiniGameWnd wnd = Create(MainWnd.g_Self.transform, "ResGUI/MiniGameWnd/MiniGameWnd");
        wnd.LevelID = level;
        wnd.m_MiniGameData = _MINI_GAME_.GetData(wnd.LevelID);
        wnd.GenerateLevel();
        
        return wnd;
    }

    public bool m_IsPause = true;
    public const float TIMEPERFRAME = 0.02f;
    public const float MOVETIME = 0.3f;
    float m_timer = 0f;
    public uint LevelID;
    public UITexture m_BG;
    public RunningObjManager m_RunManager;
    private List<_MINI_GAME_> m_MiniGameData;
    public PlayerObj m_Player;
    public readonly Vector3 MID = new Vector3(0, -462, 0);
    public readonly Vector3 LEFT = new Vector3(-270, -462, 0);
    public readonly Vector3 RIGHT = new Vector3(270, -462, 0);
    private Road m_PlayerPosition;
    private bool m_isTouching;
    private Vector3 m_touchStartPos;
    public GameObject m_Sensor;
    public GameObject m_BackBtn;
    public UILabel m_Label;
    private float m_SpeedUpTime=1f;
    private float m_RunTime = 0f;
    private bool m_isResume = true;
    public float m_GoalTimer = 0;
    public bool m_IsLose = false;
    private void GenerateLevel()
    {
        m_BG.mainTexture = Resources.Load<Texture2D>("MiniGame/BG/" + m_MiniGameData[0].LevelBG);
        m_BG.MakePixelPerfect();
        m_SpeedUpTime = m_MiniGameData[0].SpeedUp;
        m_Player = PlayerObj.Create(transform, "player1", MID);
        m_PlayerPosition = Road.Middle;
        m_pathnow = GetRandomPath(m_MiniGameData);
        m_totalWeight = CalculateTotalWeight(m_MiniGameData);
        m_GoalTimer = m_MiniGameData[0].WinTime;
        m_Player.ObjShow(true);
    }
   private int m_totalWeight = int.MaxValue;

   private int CalculateTotalWeight(List<_MINI_GAME_> x)
   {
          int Weight = 0;
           foreach (_MINI_GAME_ mini in x)
           {
               Weight += mini.Power;
           }
           return Weight;
   }
   public List<_PATH_> GetRandomPath(List<_MINI_GAME_> x)
   {
       int random = Mathf.FloorToInt(UnityEngine.Random.value * m_totalWeight);
       foreach (_MINI_GAME_ mini in x)
       {
           if (random <= mini.Power)
           {
               return _PATH_.GetData(mini.PathID);
           }
           random -= mini.Power;
       }
       m_totalWeight = CalculateTotalWeight(x);
       return _PATH_.GetData(x[0].PathID);
   }

    protected override void Ini()
    {
        base.Ini();
        UIEventListener.Get(m_Sensor).onDragStart = (obj) =>
        {
            onDragStart();
        };
        UIEventListener.Get(m_Sensor).onDragEnd = (obj) =>
        {
            onDragEnd();
        };
        UIEventListener.Get(m_BackBtn).onClick = (obj) =>
         {
             if (!m_IsLose)
             {
                 m_IsPause = true;
                 YesNoWnd.Create("請問是否返回主頁面？", () =>
                 {
                     ChangeWnd.LoadWnd<WorkWnd>(this);
                 }, () =>
                 {
                     Resume();
                 });
             }
         };
    }
    float spawntime = 0;

    public void Resume()
    {
        m_isResume = true;
        startuptimer = 0;
    }

    float startuptimer = 0;
    private float m_NextSpawnTime = 1f;
    public int m_pathIndex = 0;
    public List<_PATH_> m_pathnow;
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_isResume)
        {
            startuptimer += Time.fixedDeltaTime;
            if (startuptimer <= 1)
            {
                m_Label.text = "3";
            }
            else if (startuptimer <= 2)
            {
                m_Label.text = "2";
            }
            else if (startuptimer <= 3)
            {
                m_Label.text = "1";
            }
            else if (startuptimer <= 4)
            {
                m_Label.text = "Start!";
            }
            else if (startuptimer <= 5)
            {
                m_Label.text = "";
                m_isResume = false;
                m_IsPause = false;
            }

        }
        if (!m_IsLose)
        {
            if (!m_IsPause)
            {
                spawntime += Time.fixedDeltaTime;
                m_RunTime += Time.fixedDeltaTime;
                m_Label.text = Mathf.FloorToInt(m_RunTime).ToString();
                if(m_RunTime >= m_GoalTimer)
                {
                    m_IsPause = true;
                    TipWnd.Create("恭喜獲勝！獲得了" + m_MiniGameData[0].Reward.ToString() +"個月的工資！", () =>
                    {
                        SNumber S = m_MiniGameData[0].Reward* GData.g_Inventory.m_LevelNow.Salary ;
                        GData.g_Inventory.GainMoney(S);

                        ChangeWnd.LoadWnd<WorkWnd>(this);
                    });
                }
                if (spawntime >= m_NextSpawnTime)
                {
                    spawntime = 0;
                    m_RunManager.Spawn(m_pathnow[m_pathIndex]);
                    m_NextSpawnTime = m_pathnow[m_pathIndex].NextPathDelay;
                    m_pathIndex++;
                    if (m_pathIndex >= m_pathnow.Count)
                    {
                        m_pathIndex = 0;
                        m_pathnow = GetRandomPath(m_MiniGameData);
                    }
                }


                m_timer += Time.fixedDeltaTime * m_SpeedUpTime;
                if (m_timer >= TIMEPERFRAME)
                {
                    m_timer -= TIMEPERFRAME;
                    m_RunManager.Run(TIMEPERFRAME * m_SpeedUpTime);
                }
            }
        }
    }
    void onDrag()
    {
        float deltaX = m_touchStartPos.x - Input.mousePosition.x;
        if (Mathf.Abs(deltaX) > 30)
        {
            m_isTouching = false;
            if(deltaX >0)
            {
                if(m_PlayerPosition == Road.Middle)
                {
                    m_PlayerPosition = Road.Left;
                    TweenPosition.Begin(m_Player.gameObject, MOVETIME/m_SpeedUpTime, LEFT);
                }
                else if(m_PlayerPosition == Road.Right)
                {
                    m_PlayerPosition = Road.Middle;
                    TweenPosition.Begin(m_Player.gameObject, MOVETIME / m_SpeedUpTime, MID);
                }
            }
            else
            {
                if (m_PlayerPosition == Road.Middle)
                {
                    m_PlayerPosition = Road.Right;
                    TweenPosition.Begin(m_Player.gameObject, MOVETIME / m_SpeedUpTime, RIGHT);
                }
                else if (m_PlayerPosition == Road.Left)
                {
                    m_PlayerPosition = Road.Middle;
                    TweenPosition.Begin(m_Player.gameObject, MOVETIME / m_SpeedUpTime, MID);
                }
                //goright
            }
        }
    }
    void onDragStart()
    {
        if (m_isTouching == false)
        {
            m_isTouching = true;
            m_touchStartPos = Input.mousePosition;
        }
    }

    void onDragEnd()
    {
        m_isTouching = false;
    }

  protected override void Update()
    {
        if (m_isTouching == true) onDrag();
    }

    public enum Road
    {
        Left =0,
        Middle =1,
        Right =2,
    }

    public void Lose()
    {
        m_IsPause = true;
    }

}
