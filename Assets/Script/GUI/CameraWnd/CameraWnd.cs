﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
public class CameraWnd : GeneralWnd<CameraWnd>
{
    protected WebCamTexture m_frontWebCam;
    public UITexture m_Texture;
    public UISprite m_Overlay;
    public GameObject m_CaptureBtn;
    public UITexture m_Preview;
    public TweenAlpha m_Flash;
    public GameObject m_DonWantGetCapture;
    public static CameraWnd Create()
    {
        return Create(MainWnd.g_Self.transform, "ResGUI/CameraWnd/CameraWnd");
    }
    CaptureAndSave snapShot;

    void OnEnable()
    {
        CaptureAndSaveEventListener.onScreenShotInvoker += OnScreenShot;
    }

    void OnDisable()
    {
        CaptureAndSaveEventListener.onScreenShotInvoker -= OnScreenShot;
    }
    void OnScreenShot(Texture2D tex2D)
    {
        m_Preview.mainTexture = tex2D;
        m_Preview.enabled = true;
        MainMenuButtonObj t = MainMenuButtonObj.Create("返回", m_Preview.transform, new Vector2(200, 130), new Vector3(-240, 563, 0));
        t.ObjShow(true);
        UIEventListener.Get(t.gameObject).onClick = (go) =>
        {
            ClosePreview();
            t.DestroySelf();
        };
        ShareWnd.ShareImage(tex2D).ObjShow(true);
        snapShot.SaveTextureToGallery(tex2D);
    }

    protected override void Ini()
    {
        base.Ini();

        MainMenuButtonObj t = MainMenuButtonObj.Create("返回", m_DonWantGetCapture.transform, new Vector2(200, 130), new Vector3(-240, 563, 0));
        t.ObjShow(true);
        UIEventListener.Get(t.gameObject).onClick = (go) =>
        {
            ChangeWnd.LoadWnd<MainMenuWnd>(this);
        };

      
        Camera.main.clearFlags = CameraClearFlags.Depth;
        snapShot = GameObject.FindObjectOfType<CaptureAndSave>();
     
        UIEventListener.Get(m_CaptureBtn).onClick += (go) =>
        {
            Capture();
        };

        UIEventListener.Get(m_Preview.gameObject).onClick += (go) =>
        {
            ClosePreview();
        };
    }


    public void Capture()
    {
        NGUITools.SetActive(m_DonWantGetCapture, false);
        m_Flash.PlayForward();
        EventDelegate.Add(m_Flash.onFinished, new EventDelegate(() =>
        {
            m_Flash.PlayReverse();
            EventDelegate.Add(m_Flash.onFinished, new EventDelegate(() =>
              {
                  snapShot.GetFullScreenShot();
              }), true);
        }), true);
    }
    
    public void ClosePreview()
    {
        m_Preview.enabled = false;
        NGUITools.SetActive(m_DonWantGetCapture, true);
    }

    protected override void CloseObj()
    {
        base.CloseObj();
        Camera.main.clearFlags = CameraClearFlags.SolidColor;
    }

}
