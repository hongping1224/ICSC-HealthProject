﻿using UnityEngine;
using System.Collections;
using System;

public class TipWnd : GeneralWnd<TipWnd>
{
	public UISprite m_Box;
	public UILabel m_Comment;
	public GameObject m_OKEvent;
    public UIPanel m_Panel;
	public static TipWnd Create(string pMessage, Action clickOKCallBack = null, int panelDepth = 999)
	{
        var aWnd = TipWnd.Create(MainWnd.g_Self.transform, "ResGUI/TipWnd/TipWnd");
		aWnd.m_Comment.text = pMessage;
		aWnd.ObjShow(true);
        aWnd.m_Panel.depth = panelDepth;
		UIEventListener.Get(aWnd.m_OKEvent).onClick = (go) =>
		{
			if(clickOKCallBack != null)
				clickOKCallBack();
			DestroyWnd();
		};
        return aWnd;
	}

	protected override void Update()
	{
		base.Update();

	}

	public void SetBgWH()
	{
		int nLine = m_Comment.height / m_Comment.fontSize;
		m_Box.height = (((m_Comment.fontSize+m_Comment.spacingY) * nLine))+100 + 125 + 160; // font size +fontTopSpacing+ button size +button spacing
		//int nCharNum = m_Comment.width / m_Comment.fontSize;
		//m_Box.width = (m_Comment.fontSize * (nCharNum + 10));
        m_Comment.transform.localPosition = new Vector3(0,0,-10);
        m_OKEvent.transform.localPosition = new Vector3(0, -m_Box.height / 2.0f + 162.5f, -10);
		//m_CancelBtn.transform.localPosition = new Vector3(-70, -m_Box.height / 2.0f + 75, -10);
	}

	protected override void Ini()
	{
		base.Ini();
		SetBgWH();
	}

	protected override void IniTransform()
	{
		transform.localPosition = new Vector3(0, 0, -2600);
		transform.localEulerAngles = Vector3.zero;
		transform.localScale = Vector3.one;
	}
}
