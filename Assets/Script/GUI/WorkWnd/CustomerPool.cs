﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CustomerPool : MonoBehaviour
{
    public static CustomerPool Instant;

    public void Awake()
    {
        Instant = this;
    }
    public CustomerObj GetCustomer(Transform t)
    {
        if (transform.childCount == 0)
        {
            CustomerObj.Create(transform, Vector3.zero, Vector3.zero, 0f).transform.parent = transform;
        }
        CustomerObj c = transform.GetChild(0).GetComponent<CustomerObj>();
        c.transform.parent = t;
        return c;
    }

    public void StoreCustomer(CustomerObj c)
    {
        StartCoroutine(CustomerEndingAnimation(c));
    }
    public IEnumerator CustomerEndingAnimation(CustomerObj c)
    {
        /// playing animation
        yield return null;
        c.transform.parent = transform;
        c.ObjShow(false);

    }

}
