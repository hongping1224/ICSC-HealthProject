﻿using UnityEngine;
using System.Collections;

public class NumberJumpObj : GeneralObj
{
    public UILabel m_Label;
    public TweenPosition m_TweenPostion;
    public TweenAlpha m_TweenAlpha;
    private static float m_duration = 2f;

    public static NumberJumpObj Create(Transform parent , string text , Vector3 position)
    {
        NumberJumpObj nobj = CreateObj<NumberJumpObj>(parent , "ResGUI/WorkWnd/NumberJumpObj",position);
        nobj.m_Label.text = text;
        nobj.m_TweenPostion.from = position;
        nobj.m_TweenPostion.to = position + new Vector3(0, 30, 0);
        nobj.m_TweenPostion.duration = m_duration;
        nobj.m_TweenAlpha.duration = m_duration;
        nobj.m_TweenAlpha.from = 1f;
        nobj.m_TweenAlpha.to = 0f;
        return nobj;
    }
    protected override void Ini()
    {
        base.Ini();
        m_TweenAlpha.PlayForward();
        m_TweenPostion.PlayForward();
        m_TweenPostion.AddOnFinished(new EventDelegate(DestroySelf));
    }


}
