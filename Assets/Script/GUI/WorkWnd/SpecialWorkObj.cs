﻿using UnityEngine;
using System.Collections;
using System;
public class SpecialWorkObj : GeneralObj
{
    public Action OnClicked;
    private bool m_isRunning;
    private Vector3 m_startingPos= new Vector3(2000, 0, 0);
    public static SpecialWorkObj Create(Transform parent)
    {
        return CreateObj<SpecialWorkObj>(parent, "ResGUI/WorkWnd/SpecialWorkObj",new Vector3(2000,0,0));
    }

    protected override void Ini()
    {
        base.Ini();
        UIEventListener.Get(gameObject).onClick = (go) =>
            {
                m_isRunning = false;
                if (OnClicked != null)
                    OnClicked();
            };
    }

    protected override void RefreshObj()
    {
        base.RefreshObj();
        if (m_isRunning == false)
        {
            m_isRunning = true;
            transform.localPosition = m_startingPos;
            EventDelegate.Add(TweenPosition.Begin(gameObject, 10f, -m_startingPos ).onFinished, new EventDelegate(() =>
            {
                m_isRunning = false;
            }), true);

        }
    }
    protected override void CloseObj()
    {
        base.CloseObj();
        m_isRunning = false;
        transform.localPosition = m_startingPos;
    }
  
}
