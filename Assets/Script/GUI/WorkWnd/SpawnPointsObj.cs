﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SpawnPointsObj {

    public List<Vector3> SpawnPoints =new List<Vector3>();

    public void GenerateSpawnPointRectangle()
    {
        //left
        for (int i = 0; i < 8; i++)
        {
            Vector3 t =  new Vector3(-600, 0 - (i * 100), 0);
            SpawnPoints.Add(t);
        }
        for (int i = 0; i < 8; i++)
        {
            Vector3 t= new Vector3(600, 0 - (i * 100), 0);
            SpawnPoints.Add(t);
        }
        for (int i = 0; i < 13; i++)
        {
            Vector3 t  = new Vector3(-600 +(i*100),-800, 0);
            SpawnPoints.Add(t);
        }
        
    }

    public Vector3 GetRandomSpawnPoint()
    {
        return SpawnPoints[(int)((UnityEngine.Random.value * 10000000) % SpawnPoints.Count)];
    }

}
