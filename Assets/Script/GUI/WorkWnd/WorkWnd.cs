﻿using UnityEngine;
using System.Collections;
using System;
using GameDataReader;
using System.Collections.Generic;
public class WorkWnd : GeneralWnd<WorkWnd>
{
    public UILabel m_MoneyLabel;
    public UILabel m_JobName;
    public SNumber m_Salary = new SNumber(1f, 0);
    public SNumber m_ClickSalary = new SNumber(1f, 0);
    public GameObject m_MainMenuBtn;
    public UILabel m_SalaryLabel;
    public UILabel m_ClickLabel;
    private _LEVEL_ m_levelNow;
    public UILabel m_TimeLeft;
    public SpawnPointsObj m_SpawnPoints;
    public List<CustomerObj> m_CustomerObj;
    public Transform m_CustomerHolder;
    public IPassObj m_IPass;
    public HealthBarObj m_HealthBar;
    public GameObject m_GoMapButton;
    public GameObject m_ShareButton;


    private int m_rateOfSpawn=10; // smaller faster



    public static WorkWnd Create()
    {
        return Create(MainWnd.g_Self.transform, "ResGUI/WorkWnd/WorkWnd");
    }

    protected override void Ini()
    {
        base.Ini();
      
        refresh();
        UIEventListener.Get(m_MainMenuBtn).onClick = (go) =>
        {
            ChangeWnd.LoadWnd<MainMenuWnd>(this);
        };
        UIEventListener.Get(m_GoMapButton).onClick = (go) =>
        {
            ChangeWnd.LoadWnd<MapWnd>(this);
        };
        m_SpawnPoints = new SpawnPointsObj();
        m_SpawnPoints.GenerateSpawnPointRectangle();
        m_HealthBar.Initial(m_levelNow.LV_Require, GData.g_Inventory.m_Money);
    }

    private float timepass=0;
    private float timeref = 3f;

    protected override void FixedUpdate()
    {
        //run Timer for Salary
        base.FixedUpdate();
       m_HealthBar.ToValue(GData.g_Inventory.m_Money);
        m_MoneyLabel.text = "現有存款"+GData.g_Inventory.m_Money.ToString()+"元";
        timepass += Time.deltaTime;
        if (timepass >= timeref)
        {
            timepass -= timeref;
            WorkHour();
        }
        m_TimeLeft.text = Utilities.ToTimeString( Mathf.Ceil(timeref -timepass));

        //Generate Customer

        if (m_CustomerHolder.childCount < 2)
        {
            GenerateCustomer();
        }
        else if (m_CustomerHolder.childCount < 15)
        {
            int s =(int)((UnityEngine.Random.value*1000) % 1000);
            //Debug.Log(s);
            if(s <= m_rateOfSpawn)
            {
                GenerateCustomer();
            }
        }

    }   
    
    public void GenerateCustomer()
    {
        CustomerObj c = (CustomerPool.Instant.GetCustomer(m_CustomerHolder));
        c.OnClicked = () =>
            {
                m_CustomerObj.Remove(c);
                m_IPass.Work(c);
            };
        c.SetIni(m_SpawnPoints.GetRandomSpawnPoint(), GetRandomTarget(), ((UnityEngine.Random.value*2)+4f));
        c.ObjShow(true);
        m_CustomerObj.Add(c);
    }
    public Vector3 GetRandomTarget()
    {
        float randomx;
        float randomy;

        randomy = (UnityEngine.Random.value*400) - 460f;
        if (randomy >= 152.5f)
        {
            randomx = (UnityEngine.Random.value * 670) - 335f;
        }
        else
        {
            randomx = (UnityEngine.Random.value * 510) - 210f;
        }
        //Debug.Log(randomx + ","  + randomy);
        return new Vector3(randomx,randomy,0);
    }

    public void WorkHour()
    {
        work(m_Salary);
    }

    public void WorkClick(CustomerObj cus)
    {
        if (cus.randomValue <= GData.g_Inventory.m_LevelNow.Lucky)
        {
            Debug.Log("GOGO");
            work(m_ClickSalary);
            work(m_ClickSalary);
        }
        else if (cus.randomValue >= (100 - GData.g_Inventory.m_LevelNow.BadLuck))
        {
            Debug.Log("bad");
            badwork(m_ClickSalary);
        }
        else
        {
            work(m_ClickSalary);
        }
    }

    private void work(SNumber Salary)
    {
        NumberJumpObj.Create(m_IPass.transform, "+" + Salary + "元", m_IPass.m_HeadPosition).ObjShow(true);
        GData.g_Inventory.GainMoney(Salary);
        if (GData.g_Inventory.m_Money >= m_levelNow.LV_Require)
        {
            GData.g_Inventory.LevelUp();
            refresh();
            m_HealthBar.Initial(m_levelNow.LV_Require, GData.g_Inventory.m_Money);
        }
    }

    private void badwork(SNumber Salary)
    {
        NumberJumpObj.Create(m_IPass.transform, "-" + Salary + "元", m_IPass.m_HeadPosition).ObjShow(true);
        GData.g_Inventory.TrySpendMoney(Salary);
        if (GData.g_Inventory.m_Money >= m_levelNow.LV_Require)
        {
            GData.g_Inventory.LevelUp();
            refresh();
            m_HealthBar.Initial(m_levelNow.LV_Require, GData.g_Inventory.m_Money);
        }
    }

    private void refresh()
    {
        m_levelNow = GData.g_Inventory.m_LevelNow;
        m_JobName.text = m_levelNow.Name;
        m_Salary = m_levelNow.Salary;
        m_ClickSalary = m_levelNow.Click;
        m_SalaryLabel.text = "時薪" + m_Salary.ToString();
        m_ClickLabel.text = "點擊錢" + m_ClickSalary.ToString();
    }



}
