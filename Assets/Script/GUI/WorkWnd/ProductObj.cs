﻿using UnityEngine;
using System.Collections;

public class ProductObj : GeneralObj
{
    public UISprite m_Sprite;
    public TweenPosition m_TweenPosition;
    public static ProductObj Create(Transform t,Vector3 position)
    {
        return CreateObj<ProductObj>(t, "ResGUI/WorkWnd/ProductObj",position);
    }
}
