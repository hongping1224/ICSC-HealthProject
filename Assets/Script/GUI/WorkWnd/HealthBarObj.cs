﻿using UnityEngine;
using System.Collections;

public class HealthBarObj : GeneralObj
{
    public UISprite m_Frame;
    public UISprite m_Fill;
    public SNumber m_Max;
    public SNumber m_value;
    public TweenWidth m_TweenWidth;

    public static HealthBarObj Create(Transform parent, SNumber max, SNumber value,Vector3 pos)
    {
        HealthBarObj hobj = CreateObj<HealthBarObj>(parent, "ResGUI/WorkWnd/HealthBarObj",pos);
        hobj.m_Max = max;
        hobj.m_value = value;
        return hobj;
    }

    public void Initial(SNumber max, SNumber value)
    {
        m_Max = max;
        m_value = value;
    }

    public void ToValue(SNumber Value)
    {
        float percent = float.Parse((Value / m_Max).ToString());
        int targetwidth = Mathf.FloorToInt(m_Frame.width * percent);
        m_TweenWidth.from = m_Fill.width;
        m_TweenWidth.to = targetwidth;
        m_TweenWidth.ResetToBeginning();
        m_TweenWidth.PlayForward();
    }
}
