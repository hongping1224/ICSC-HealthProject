﻿using UnityEngine;
using System.Collections;
using System;
public class CustomerObj : GeneralObj
{
    public TweenPosition m_tweenPosition;
    public UISprite m_Sprite;
    public BoxCollider m_BoxC;
    public Action OnClicked;
    public int randomValue;
    public static CustomerObj Create(Transform parent, Vector3 StartPosition, Vector3 TargetPosition, float duration)
    {
        CustomerObj cc = CreateObj<CustomerObj>(parent, "ResGUI/WorkWnd/CustomerObj", StartPosition);
        cc.SetIni(StartPosition, TargetPosition, duration);
        cc.randomValue = (int)(UnityEngine.Random.value * 100);
        return cc;
    }
    public CustomerObj SetIni(Vector3 StartPosition, Vector3 TargetPosition, float duration)
    {
        transform.localPosition = StartPosition;
        m_tweenPosition.from = StartPosition;
        m_tweenPosition.to = TargetPosition;
        m_tweenPosition.duration = duration;
        m_BoxC.enabled = true;
        randomValue = (int)(UnityEngine.Random.value * 100);
        return this;
    }

    protected override void Ini()
    {
        base.Ini();
        UIEventListener.Get(gameObject).onClick = (go) =>
            {
                if (OnClicked != null)
                    OnClicked();
                m_tweenPosition.enabled = false;
                m_BoxC.enabled = false;
            };
    }

    protected override void RefreshObj()
    {
        base.RefreshObj();
        TweenAlpha.Begin(gameObject, 0.01f, 1f);
        m_tweenPosition.ResetToBeginning();
        m_tweenPosition.PlayForward();
    }
    protected override void CloseObj()
    {
        base.CloseObj();
        TweenAlpha.Begin(gameObject, 0.5f, 0f);
    }
}
