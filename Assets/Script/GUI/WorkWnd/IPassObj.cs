﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IPassObj : GeneralObj
{
    public UISprite m_Sprite;
    public Queue<CustomerObj> m_WorkQueue = new Queue<CustomerObj>();
    private bool m_isWorking= false;
    public Vector3 m_HeadPosition
    {
        get { return transform.localPosition + new Vector3(0, 50, 0); }
    }
    public void Work(CustomerObj customer)
    {
        if(m_WorkQueue.Contains(customer))
        {
            return;
        }
        m_WorkQueue.Enqueue(customer);
    }

    protected override void Update()
    {
        base.Update();

        if (m_isWorking == false)
        {
            if (m_WorkQueue.Count > 0)
            {
                m_isWorking = true;
                StartCoroutine(StartWork());
            }
        }
    }

    public IEnumerator StartWork()
    {
        //run animation
        yield return null;
        //workdone
        ThrowProduct(m_WorkQueue.Dequeue());
        m_isWorking = false;
    }

    public void ThrowProduct(CustomerObj c)
    {
        //generate obj
       ProductObj p =  ProductObj.Create(WorkWnd.g_Self.transform, transform.localPosition);
       //Debug.Log("genereate");
        //obj startmoving 
       float duration = (p.gameObject.transform.localPosition - c.transform.localPosition).magnitude/600f;
       p.m_TweenPosition.from = p.gameObject.transform.localPosition;
       p.m_TweenPosition.to = c.transform.localPosition;
       p.m_TweenPosition.duration = duration;
       p.m_TweenPosition.ResetToBeginning();
       p.m_TweenPosition.PlayForward();

       //ondone 
        
       EventDelegate.Add(p.m_TweenPosition.onFinished, new EventDelegate(() =>
        {
            //generate gold;
            WorkWnd.g_Self.WorkClick(c);
            CustomerPool.Instant.StoreCustomer(c);
            p.DestroySelf();
        }), true);
       p.ObjShow(true);
        
    }

    

  
}
