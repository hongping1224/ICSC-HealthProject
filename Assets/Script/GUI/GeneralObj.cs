﻿using UnityEngine;
using System.Collections;

public class GeneralWnd<V> : GeneralObj where V : GeneralWnd<V>
{
	public static V g_Self;

	void Awake()
	{
		g_Self = GetComponent<V>();
	}

	protected override void Start()
	{
		base.Start();
	}

	public static void DestroyWnd()
	{
		if(g_Self != null)
			g_Self.DestroySelf();
	}

	public static V Create(Transform parent, string pPath)
	{
		if(g_Self == null)
			g_Self = CreateObj<V>(parent, pPath);
		return g_Self;
	}

	public static V Create(Transform parent, string pPath, Vector3 vPos)
	{
		if(g_Self == null)
			g_Self = CreateObj<V>(parent, pPath, vPos);
		return g_Self;
	}

	public static V Create(Transform parent, Object obj)
	{
		if(g_Self == null)
			g_Self = CreateObj<V>(parent, obj);
		return g_Self;
	}
   

	public static V Create(Transform parent, Object obj, Vector3 vPos)
	{
		if(g_Self == null)
			g_Self = CreateObj<V>(parent, obj, vPos);
		return g_Self;
	}
}

public class GeneralObj : MonoBehaviour
{
	protected bool m_bIni = false;
	private Transform m_transform;
	private GameObject m_gameObject;
	private Renderer m_renderer;
    private AudioSource m_audio;

	new public Transform transform
	{
		get
		{ 
			if(m_transform == null)
				m_transform = base.transform;
			return m_transform;
		}
	}

	new public GameObject gameObject
	{
		get
		{ 
			if(m_gameObject == null)
				m_gameObject = base.gameObject;
			return m_gameObject;
		}
	}

	new public Renderer renderer
	{
		get
		{ 
			if(m_renderer == null)
				m_renderer = base.GetComponent<Renderer>();
			return m_renderer;
		}
	}

    new public AudioSource audio
    {
        get
        {
            if (m_audio == null)
                m_audio = base.GetComponent<AudioSource>();
            return m_audio;
        }
    }
    protected virtual void Start()
	{

	}

	protected virtual void FixedUpdate()
	{
		
	}

	protected virtual void Update()
	{
	
	}

	public virtual void DestroySelf()
	{
		ObjShow(false);
		GameObject.DestroyImmediate(this.gameObject);
	}

	///介面開關
	public virtual void ObjShow(bool bShow)
	{
		if(bShow)
		{
			if(!m_bIni)
			{
				Ini();
				m_bIni = true;
			}
			RefreshObj();
		}
		else
		{
			CloseObj();
		}
		gameObject.SetActive(bShow);
	}

	///放置物件初始化的Code 物件生成後只會執行一次(執行ObjShow(true)時)
	protected virtual void Ini()
	{

	}

	///放開啟物件的時候的Code 每次執行 ObjShow(true)時都會執行
	protected virtual void RefreshObj()
	{

	}

	///放關閉物件的時候的Code 每次執行 ObjShow(false)時都會執行
	protected virtual void CloseObj()
	{
		
	}

	public virtual void Refresh()
	{

	}

	protected virtual void IniTransform()
	{
		this.GameObjectIni();
	}

	protected virtual void IniTransform(Vector3 vPos)
	{
		this.GameObjectIni(vPos);
	}

	public static V CreateObj<V>(Transform parent, string pPath, Vector3? vPos) where V : GeneralObj
	{
		V aObj = (GameObject.Instantiate(Resources.Load(pPath)) as GameObject).GetComponent<V>();
		aObj.transform.parent = parent;
		if(vPos.HasValue)
			aObj.IniTransform(vPos.Value);
		else
			aObj.IniTransform();
		aObj.gameObject.SetActive(false);
		return aObj;
	}

	public static V CreateObj<V>(Transform parent, string pPath) where V : GeneralObj
	{
		return CreateObj<V>(parent, pPath, null);
	}

	public static V CreateObj<V>(Transform parent, Object obj, Vector3? vPos) where V : GeneralObj
	{
		V aObj = (GameObject.Instantiate(obj)) as V;
		aObj.transform.parent = parent;
		if(vPos.HasValue)
			aObj.IniTransform(vPos.Value);
		else
			aObj.IniTransform();
		aObj.gameObject.SetActive(false);
		return aObj;
	}

	public static V CreateObj<V>(Transform parent, Object obj) where V : GeneralObj
	{
		return CreateObj<V>(parent, obj, null);
	}
  
}
