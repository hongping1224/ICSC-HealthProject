﻿using UnityEngine;
using System.Collections;

public class ShareWnd : GeneralWnd<ShareWnd>
{
    public UITexture m_UITexture;
    private Texture2D m_ShareImage;
    public GameObject m_ShareButton;
    private uint m_ShareLevel;
    public static ShareWnd Create ()
    {
        ShareWnd s = Create(MainWnd.g_Self.transform,"ResGUI/ShareWnd/ShareWnd");
        return s;
    }
    public static ShareWnd ShareLevel(uint level)
    {
        ShareWnd s = Create();
        return s;
    }

    public static ShareWnd ShareImage(Texture2D image)
    {
        ShareWnd s = Create();
        s.m_ShareImage = image;
        s.m_UITexture.mainTexture = image;
        s.m_UITexture.SetDimensions(720,1280);
        return s;
    }

    protected override void Ini()
    {
        base.Ini();
    }

  

}
