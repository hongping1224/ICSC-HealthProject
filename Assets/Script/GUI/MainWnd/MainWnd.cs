﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using GameDataReader;
public class MainWnd : GeneralWnd<MainWnd>
{
    private UIPanel m_MainPanel;
    public UILabel m_Debug;
    public enum Mode
    {
        Real,
        MainWnd,
        CameraWnd,
        WorkWnd,
        MapWnd,
        MiniGameWnd,
    }
    public Mode PlayMode;
    protected void Awake()
    {

        if (m_MainPanel == null)
            m_MainPanel = GetComponent<UIPanel>();

        g_Self = this;

      //  SPFacebook.Instance.Init();
    }

    protected override void Start()
    {
        base.Start();
        ObjShow(true);
    }

    public static MainWnd Create()
    {
        return Create(Camera.main.transform.root, "ResGUI/MainWnd/MainWnd");
    }

    public UIPanel GetMainPanel()
    {
        if (m_MainPanel == null)
            m_MainPanel = GetComponent<UIPanel>();
        return m_MainPanel;
    }
    protected override void Ini()
    {
        base.Ini();
        //INITIALIZE XMLDATA
        XmlData.GetXmlTextReader = (filename) =>
        {
            TextAsset data = null;
            data = (TextAsset)Resources.Load("setting/" + filename) as TextAsset;
            StringReader stringReader = new StringReader(data.text);
            XmlTextReader reader = new XmlTextReader(stringReader);
            return reader;
        };
        XmlData.ReadXmlData();
        System.GC.Collect();
        Resources.UnloadUnusedAssets();

        TimeConnection con = new TimeConnection();

        con.SentRequest();


#if BRAC_SYMBOL
        Debug.Log("BRAC");
        //Connect to server Get Data;
#endif
        con.OnError += (e) =>
            {
                Debug.Log(e);
            };
        con.OnSuccess += () =>
            {
                AudioManager.Create().ObjShow(true);
                AudioManager.g_Self.PlayBGM("MainBGM");
                switch (PlayMode)
                {
                    case Mode.Real:
                        LoginWnd.Create().ObjShow(true);
                        break;
                    case Mode.MainWnd:
                        LoginWnd.Create().ObjShow(true);
                        break;
                    case Mode.CameraWnd:
                        CameraWnd.Create().ObjShow(true);
                        break;
                    case Mode.WorkWnd:
                        WorkWnd.Create().ObjShow(true);
                        break;
                    case Mode.MapWnd:
                        MapWnd.Create().ObjShow(true);
                        break;
                    case Mode.MiniGameWnd:
                        MiniGameWnd.Create(999);
                        MiniGameWnd.g_Self.ObjShow(true);
                        break;
                }
            };
    }

}
