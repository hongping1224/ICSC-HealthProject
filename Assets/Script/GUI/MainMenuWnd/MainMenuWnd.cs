﻿using UnityEngine;
using System.Collections;
using System;
public class MainMenuWnd : GeneralWnd<MainMenuWnd>
{
    public static MainMenuWnd Create()
    {
        return Create(MainWnd.g_Self.transform, "ResGUI/MainMenuWnd/MainMenuWnd");
    }

    protected override void Ini()
    {
        base.Ini();

       MainMenuButtonObj t = MainMenuButtonObj.Create("返回", transform, new Vector2(200,130),new Vector3(-240,563,0));
       t.ObjShow(true);
       UIEventListener.Get(t.gameObject).onClick = (go) =>
        {
            ChangeWnd.LoadWnd<WorkWnd>(this);
        };

        string[] s = new string[4] { "相簿", "成就", "賬號設定", "製作團隊/合作公司" };
        Vector2 size = new Vector2(650, 200);
        MainMenuButtonObj[] con = new MainMenuButtonObj[6];
        for (int i = 0; i < s.Length; ++i)
        {
            con[i] = MainMenuButtonObj.Create(s[i], transform, size,new Vector3(0f, (365f - (205f * i)), 0f));
        }
        con[4] = MainMenuButtonObj.Create("FB粉絲團", transform, new Vector2(325, 130),new Vector3(-170, -429, 0));
        con[5] = MainMenuButtonObj.Create("周邊產品", transform, new Vector2(325, 130), new Vector3(170, -429, 0));
        con[0].OnClick += () =>
        {
            ChangeWnd.LoadWnd<CameraWnd>(this);
        };

        for (int i = 0; i < con.Length; ++i)
        {
            con[i].ObjShow(true);
        }
    }
}