﻿using UnityEngine;
using System.Collections;
using System;
public class MainMenuButtonObj : GeneralObj
{
    public UISprite m_BG;
    public UILabel m_Text;
    public Action OnClick;
    public BoxCollider m_BoxC;
    public static MainMenuButtonObj Create(string label,Transform parent, Vector2 Size,Vector3 Position, Action CallBack = null)
    {
        MainMenuButtonObj m = CreateObj<MainMenuButtonObj>(parent,"ResGUI/MainMenuWnd/MainMenuButtonObj",Position);
        m.m_Text.text = label;
        m.m_BG.SetDimensions((int)(Size.x), (int)(Size.y));
        m.m_BoxC.size = Size;
        UIEventListener.Get(m.gameObject).onClick = (go) =>
        {
            if (m.OnClick != null)
                m.OnClick();
        };
        return m;
    }

    public static MainMenuButtonObj Create(string label, Transform parent, Vector2 Size, Action CallBack = null)
    {
        return Create(label, parent, Size, Vector3.zero, CallBack);
    }
}
