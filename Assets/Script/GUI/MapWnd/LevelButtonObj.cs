﻿using UnityEngine;
using System.Collections;

public class LevelButtonObj : GeneralObj
{
    public static LevelButtonObj Create(Transform parent,float x, float y)
    {
        return CreateObj<LevelButtonObj>(parent, "ResGUI/MapWnd/LevelButtonObj",new Vector3(x,y));
    }
}
