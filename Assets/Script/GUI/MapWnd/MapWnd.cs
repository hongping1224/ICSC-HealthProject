﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataReader;
public class MapWnd : GeneralWnd<MapWnd>
{

    public static MapWnd Create()
    {
        MapWnd map = Create(MainWnd.g_Self.transform,"ResGUI/MapWnd/MapWnd");
        map.m_PageNow = 1;
        return map;
    }
    public uint m_PageNow;
    public GameObject m_MainMenuBtn;
    public GameObject m_BGCast;
    public UITexture m_BG;
    public UITexture m_Animation;
    public Transform m_LevelButtonCast;
    public List<UISprite> m_LevelButton;
    public GameObject m_BackButton;
    public UILabel m_APLeft;
    public UILabel m_NextApRegen;
    public GameObject m_ObjectHideOnAni;
    public GameObject m_NextButton;
    public GameObject m_PreviosButton;
    public void Test()
    {
        GData.g_Inventory.TryUseAP(2);
    }
    protected override void Ini()
    {
        base.Ini();

        UIEventListener.Get(m_MainMenuBtn).onClick = (go) =>
        {
            ChangeWnd.LoadWnd<MainMenuWnd>(this);
        };
        UIEventListener.Get(m_NextButton).onClick = (go) =>
        {
            NextPage();
        };
        UIEventListener.Get(m_PreviosButton).onClick = (go) =>
        {
            PreviosPage();
        };
        UIEventListener.Get(m_BackButton).onClick = (go) =>
        {
            ChangeWnd.LoadWnd<WorkWnd>(this);
        };
        m_PageNow = 1;
        GeneratePage(m_PageNow);
        RefreshAPDetail();
    }
    int apnow = 0;
    float dt = 0;
    protected override void Update()
    {
        base.Update();
        dt += Time.deltaTime;
        if (dt >= 1f)
        {
            dt = 0;
            RefreshAPDetail();
        }
    }

   public void RefreshAPDetail()
    {
        if (apnow != GData.g_Inventory.m_AP)
        {
            apnow = GData.g_Inventory.m_AP;
            m_APLeft.text = "選擇行動 剩餘" + apnow + "次";
        }
        if (GData.g_Inventory.m_AP != GameData.MAXAP)
        {
            m_NextApRegen.text = "恢復時間需要" + Utilities.ToTimeString((float)(GData.g_Inventory.m_nextApRegen - System.DateTime.Now).TotalSeconds + 1);
        }
        else
        {
            m_NextApRegen.text = "AP已滿";
        }
    }

    public void NextPage()
    {
        if (true)
        {
            m_PageNow++;
            m_Animation.transform.localPosition = new Vector3(m_BG.transform.localPosition.x+(m_BG.width)-1, m_BG.transform.localPosition.y);
            m_Animation.mainTexture = LoadMapTexture(m_PageNow);
            HideObject();
            Swap();
            TweenPosition tp = TweenPosition.Begin(m_BGCast, 0.5f, new Vector3(m_BGCast.transform.localPosition.x-m_BG.width, 0, 0));
            GeneratePage(m_PageNow);
            EventDelegate.Add(tp.onFinished, new EventDelegate(ShowObject), true);
        }     
    }

    public void PreviosPage()
    {
        if (m_PageNow >1)
        {
            m_PageNow--;
            m_Animation.transform.localPosition = new Vector3(m_BG.transform.localPosition.x - (m_BG.width) + 1, m_BG.transform.localPosition.y);
            m_Animation.mainTexture = LoadMapTexture(m_PageNow);
            HideObject();
            Swap();
            TweenPosition tp = TweenPosition.Begin(m_BGCast, 0.5f, new Vector3(m_BGCast.transform.localPosition.x + m_BG.width, 0, 0));
            GeneratePage(m_PageNow);
            EventDelegate.Add(tp.onFinished, new EventDelegate(ShowObject), true);
        }     
    }

    public void HideObject()
    {
        NGUITools.SetActive(m_ObjectHideOnAni, false);
    }
    public void ShowObject()
    {
        NGUITools.SetActive(m_ObjectHideOnAni, true);
    }

    public void GeneratePage(uint page)
    {
        m_LevelButtonCast.DestroyChildren();
        foreach (_MAP_ ma in _MAP_.GetData(page))
        {
            LevelButtonObj cast =   LevelButtonObj.Create(m_LevelButtonCast, ma.X, ma.Y);
            cast.ObjShow(true);
            UIEventListener.Get(cast.gameObject).onClick = (go) =>
                {
                    if (GData.g_Inventory.TryUseAP(1))
                    {
                        ChangeWnd.Create();
                        ChangeWnd.g_Self.FadeInCallBack += () =>
                            {
                                DestroySelf();
                                MiniGameWnd.Create(999);
                                MiniGameWnd.g_Self.ObjShow(true);
                                ChangeWnd.g_Self.DestroySelf();
                            };
                         ChangeWnd.g_Self.ObjShow(true);

                    }
                    else
                    {
                        TipWnd.Create("呵呵,你AP不夠,笑你");
                    }
                };
        }
    }
    public void Swap()
    {
        UITexture tmp = m_BG;
        m_BG = m_Animation;
        m_Animation = tmp;
    }

    public Texture2D LoadMapTexture(uint page)
    {
        return Resources.Load<Texture2D>("Map/map"+page);
    }

}
