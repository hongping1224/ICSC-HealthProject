﻿using UnityEngine;
using System.Collections;
using System;

public class ChangeWnd : GeneralWnd<ChangeWnd>
{
    public TweenAlpha m_TweenAlpha;
    private string[] paths;
    private GeneralObj cast;
    public Action OnFinishLaod;
    public Action FadeInCallBack;
    public static ChangeWnd Create()
    {
        return ChangeWnd.Create(MainWnd.g_Self.transform, "ResGUI/ChangeWnd/ChangeWnd");
    }
    public static ChangeWnd LoadWnd<T>(GeneralObj now , Action CallBack =null)where T : GeneralWnd<T>
    {
        ChangeWnd.Create();
        ChangeWnd.g_Self.FadeInCallBack += () =>
        {
            ChangeWnd.g_Self.StartLoading<T>();
        };
        ChangeWnd.g_Self.OnFinishLaod += () =>
        {
            
            now.DestroySelf();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GeneralWnd<T>.g_Self.ObjShow(true);
            if (CallBack != null)
            {
                CallBack();
            }
        };
        g_Self.ObjShow(true);
        return g_Self;
    }
    public void StartLoading(string[] s)
    {
        paths = s;
        StartCoroutine(startlaoding());
    }
    protected override void Ini()
    {
        base.Ini();
        m_TweenAlpha.PlayForward();
        EventDelegate.Add(m_TweenAlpha.onFinished, new EventDelegate(() =>
            { 
            if(FadeInCallBack!=null)
                FadeInCallBack();
            }), true);
    }

    public void StartLoading<V>() where V : GeneralWnd<V>
    {
        paths = new string[1] { GeneratePath<V>() };
        StartCoroutine(LoadWnd<V>());
    }

    public string GeneratePath<V>() where V : GeneralWnd<V>
    {
        string s =typeof(V).ToString();
        return "ResGUI/" + s + "/" + s;
    }

    public IEnumerator startlaoding()
    {
        for (int i = 0; i < paths.Length; i++)
        {
            // start loading
            ResourceRequest rq = Resources.LoadAsync(paths[i]);
            yield return rq.isDone;
            GameObject.Instantiate((GameObject)rq.asset);
        }

        if (OnFinishLaod != null)
            OnFinishLaod();

        EventDelegate.Add(m_TweenAlpha.onFinished, new EventDelegate(() =>
        {
            DestroySelf();
        }));
        m_TweenAlpha.PlayReverse();
        //stop loading
    }
    public IEnumerator LoadWnd<T>() where T: GeneralWnd<T> 
    {
        // start loading
            ResourceRequest rq = Resources.LoadAsync(paths[0]);
            yield return rq.isDone;
            GameObject aObj = (GameObject)(GameObject.Instantiate(rq.asset));
            GeneralWnd<T>.g_Self = aObj.GetComponent<T>();
            
            if (OnFinishLaod != null)
                OnFinishLaod();

            EventDelegate.Add(m_TweenAlpha.onFinished, new EventDelegate(() =>
            {
                DestroySelf();
            }));
            m_TweenAlpha.PlayReverse();

        //stop loading
    }

}
