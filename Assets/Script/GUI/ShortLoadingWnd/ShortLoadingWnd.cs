﻿using UnityEngine;
using System.Collections;

public class ShortLoadingWnd : GeneralWnd<ShortLoadingWnd>
{

    public UIPanel m_Panel;
    public TweenAlpha m_TweenAlpha;

    public static ShortLoadingWnd Create(int depth = 9999)
    {
        ShortLoadingWnd lw = ShortLoadingWnd.Create(MainWnd.g_Self.transform, "ResGUI/ShortLoadingWnd/ShortLoadingWnd");
        lw.m_Panel.depth = depth;
        return lw;
    }

    protected override void Ini()
    {
        base.Ini();
        m_TweenAlpha.PlayForward();
    }


}
