﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
public class ConnectionHandler : MonoBehaviour {
    
    public static ConnectionHandler g_self;
    public event Action OnAllConnectionEnd;
    public event Action OnAllConnectionEndOnce;
    private Dictionary<Guid, WeakReference> m_connections = new Dictionary<Guid, WeakReference >();
    void Awake()
    {
        g_self = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void RegisterConnection(ConnectionBase cb)
    {
        WeakReference refe;
        if (m_connections.TryGetValue(cb.m_UID, out refe))
        {
            if (refe.IsAlive)
            {
                // Debug.Log("old connection still alive generate new guid");
                cb.m_UID = Guid.NewGuid();
                m_connections.Add(cb.m_UID, new WeakReference(cb));
            }
            else
            {
                //  Debug.Log("old connection dead replace old connection");
                m_connections[cb.m_UID] = new WeakReference(cb);
            }
        }
        else
        {
            m_connections.Add(cb.m_UID, new WeakReference(cb));

        }
    }

    public void UnregisterConnection()
    {
        List<Guid> removekey = new List<Guid>(); 
        foreach (Guid key in m_connections.Keys)
        {
           if( m_connections[key].IsAlive ==false)
           {
               removekey.Add(key);
           }
        }

        foreach (Guid key in removekey)
        {
            m_connections.Remove(key);
        }



        if (m_connections.Count == 0)
        {
            if (OnAllConnectionEnd != null)
            {
                OnAllConnectionEnd();
            }
            if(OnAllConnectionEndOnce!=null)
            {
                OnAllConnectionEndOnce();
                OnAllConnectionEndOnce = null;
            }
        }
    }

    public int ConnectionCount
    {
        get { return m_connections.Count; }
        private set { }
    }
    


}
