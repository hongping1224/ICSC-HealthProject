﻿using UnityEngine;
using System.Collections;
using LitJson;
public class LoginConnection : ConnectionBase<LoginConnection> 
{
    public User m_Data;

    public override void SentRequest(WWWForm senddata)
    {
        base.SentRequest(senddata, DEFINE.URL.LOGIN);
    }

    protected override void ParsePackage(JsonData j)
    {
      
        
            m_Data = new User(j);
            GData.g_Player.m_User = m_Data;
        
        ConnectionHandler.g_self.UnregisterConnection();
        //Debug.Log(Data.ToString());
    }
}