﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
public class CaloriesDayConnection : ConnectionBase<CaloriesDayConnection>
{

    public Dictionary<ServerDate, CaloriesData> m_Data = new Dictionary<ServerDate, CaloriesData>();

    public override void SentRequest(WWWForm senddata)
    {
        base.SentRequest(senddata, DEFINE.URL.CALORIE);
    }

    protected override void ParsePackage(JsonData j)
    {
    

            IDictionary tdic = j["data"] as IDictionary;

            foreach (var t in tdic.Keys)
            {
                string ss = t.ToString();
                ServerDate date = new ServerDate(ss);
                if (!GData.g_Player.ContainCalories(date))
                {
                    m_Data.Add(date, new CaloriesData(date, j["data"][ss]));
                }
            }
            GData.g_Player.UpdateData(m_Data);
        

        ConnectionHandler.g_self.UnregisterConnection();
    }
}
