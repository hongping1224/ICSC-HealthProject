﻿using UnityEngine;
using System.Collections;

public class RegenApConnection : TimeConnection
{
    protected override void ParsePackage(string j)
    {
        base.ParsePackage(j);
        GData.g_Inventory.RefreshAp();
    }
}
