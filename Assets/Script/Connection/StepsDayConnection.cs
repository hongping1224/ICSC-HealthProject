﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
public class StepsDayConnection : ConnectionBase<StepsDayConnection>
{
    public Dictionary<ServerDate, StepsData> m_Data = new Dictionary<ServerDate, StepsData>();

    public override void SentRequest(WWWForm senddata)
    {
        base.SentRequest(senddata, DEFINE.URL.STEP);

    }

    protected override void ParsePackage(JsonData j)
    {
 
            IDictionary tdic = j["data"] as IDictionary;

            foreach (var t in tdic.Keys)
            {
                string ss = t.ToString();
                ServerDate date = new ServerDate(ss);
                if (!GData.g_Player.ContainSteps(date))
                {
                    m_Data.Add(date, new StepsData(date, j["data"][ss]));
                }
            }
            GData.g_Player.UpdateData(m_Data);
        

        ConnectionHandler.g_self.UnregisterConnection();
    }


}
