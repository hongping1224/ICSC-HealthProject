﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
public class StepsHourConnection : ConnectionBase<StepsHourConnection>
{
    public Dictionary<ServerDate, StepsData> m_Data = new Dictionary<ServerDate, StepsData>();
    public ServerDate m_Date;

    public void SentRequest(WWWForm senddata,ServerDate requestdate)
    {
        m_Date = requestdate;
        base.SentRequest(senddata, DEFINE.URL.STEP);
    }

    protected override void ParsePackage(JsonData j)
    {
            m_Data.Add(m_Date, new StepsData(m_Date, j["data"], true));
            GData.g_Player.UpdateData(m_Data);
        

        ConnectionHandler.g_self.UnregisterConnection();
    }


}
