﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class ActiveConnection : ConnectionBase<ActiveConnection>
{

    public Dictionary<ServerDate, ActiveData> m_Data = new Dictionary<ServerDate, ActiveData>();

    public override void SentRequest(WWWForm senddata)
    {
        //   date = requestdate;
        base.SentRequest(senddata, DEFINE.URL.ACTIVE);
    }

    protected override void ParsePackage(JsonData j)
    {
        if (j["data"] != null)
        {
            IDictionary tdic = j["data"] as IDictionary;
            foreach (var t in tdic.Keys)
            {
                string ss = t.ToString();
                ServerDate date = new ServerDate(ss);
                m_Data.Add(date, new ActiveData(date, j["data"][ss]));
                // Debug.Log(Data[date].ToString());
            }
            GData.g_Player.UpdateData(m_Data);
        }  

        ConnectionHandler.g_self.UnregisterConnection();
    }


}
