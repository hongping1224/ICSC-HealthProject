﻿using UnityEngine;
using System.Collections;
using System;
using LitJson;

public class ConnectionBase
{
    public Guid m_UID;
   
}

public class ConnectionBase<T> : ConnectionBase where T : ConnectionBase<T>
{
    
  //  protected string ReturnData;
    public event Action<JsonData> OnPackageRecieve;
    public event Action<string> OnPackageRecieved;
    public event Action OnSuccess;
    public event Action<string> OnError;
    public event Action OnConnectionEnd;

    public static V Create<V>(Guid uid) where V : ConnectionBase<V>
    {
        V t = Activator.CreateInstance(typeof(V)) as V;
        t.m_UID = uid;
        return t;
    }

    public static T Create()
    {
        return Create<T>(Utilities.lUnique);
    }

    public virtual void SentRequest(WWWForm senddata)
    {
        SentRequest(senddata, DEFINE.URL.TEST);
    }
    public virtual void SentRequest(WWWForm senddata, string url)
    {
        ConnectionHandler.g_self.RegisterConnection(this);
        ConnectionHandler.g_self.StartCoroutine(Send(url, senddata));
        OnConnectionEnd += () =>
            {
                ConnectionHandler.g_self.UnregisterConnection();
            };
        OnPackageRecieve += ParsePackage;

    }
    protected IEnumerator Send(string url, WWWForm senddata)
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            WWW www = new WWW(url, senddata);
            yield return www;

            if (www.error != null)
            {
                if (OnError != null)
                {
                    OnError(www.error);
                }
            }
            else if (OnPackageRecieve != null)
            {
                JsonData j = JsonMapper.ToObject(www.text);
                if (j["info"].ToString() == "success")
                {
                    OnPackageRecieve(j);
                    if (OnSuccess != null)
                        OnSuccess();
                }
                else
                {
                    if (OnError != null)
                        OnError(www.text);
                }
            }
            if (OnConnectionEnd != null)
                OnConnectionEnd();
        }
        else
        {
            yield return null;
            if (OnError != null)
                OnError("no Connection");
            if (OnConnectionEnd != null)
                OnConnectionEnd();
        }
    }
    protected IEnumerator Send(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        if (www.error != null)
        {
            if (OnError != null)
            {
                OnError(www.error);
            }
        }
        else if (OnPackageRecieved != null)
        {
                OnPackageRecieved(www.text);
                if (OnSuccess != null)
                    OnSuccess();
        }
        if (OnConnectionEnd != null)
            OnConnectionEnd();
    }
    protected virtual void ParsePackage(JsonData j)
    {
       
    }




}