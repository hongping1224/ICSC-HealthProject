﻿using UnityEngine;
using System.Collections;
using LitJson;

public class SaveConnection : ConnectionBase<SaveConnection>
{
    public void SentRequest()
    {
        ConnectionHandler.g_self.RegisterConnection(this);
        ConnectionHandler.g_self.StartCoroutine(Send(DEFINE.URL.TIME));
        OnConnectionEnd += () =>
        {
            ConnectionHandler.g_self.UnregisterConnection();
        };
        OnPackageRecieved += ParsePackage;
    }

    protected override void ParsePackage(JsonData j)
    {
        Debug.Log("Saved");
        GData.g_date = new ServerDate(j.ToString());
        ConnectionHandler.g_self.UnregisterConnection();
        //Debug.Log(Data.ToString());
    }
    protected virtual void ParsePackage(string j)
    {
        Debug.Log("Saved");
        GData.g_date = new ServerDate(j);
        ConnectionHandler.g_self.UnregisterConnection();
        //Debug.Log(Data.ToString());
    }
}
