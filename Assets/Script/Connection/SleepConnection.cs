﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class SleepConnection : ConnectionBase<SleepConnection>
{
    public Dictionary<ServerDate, SleepData> m_Data = new Dictionary<ServerDate,SleepData>();
    
    public override void SentRequest(WWWForm senddata)
    {
        base.SentRequest(senddata,DEFINE.URL.SLEEP);
    }

    protected override void ParsePackage(JsonData j)
    {

                IDictionary tdic = j["data"] as IDictionary;
                    m_Data = new Dictionary<ServerDate, SleepData>();
                    foreach (var t in tdic.Keys)
                    {
                        string ss = t.ToString();
                        ServerDate date = new ServerDate(ss);
                        m_Data.Add(date, new SleepData(date, j["data"][ss]));
                    }
                 GData.g_Player.UpdateData(m_Data);
         
            ConnectionHandler.g_self.UnregisterConnection();
    }

}
