﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class DistanceConnection : ConnectionBase<DistanceConnection>
{
    public Dictionary<ServerDate, int> m_Data = new Dictionary<ServerDate, int>();

    public override void SentRequest(WWWForm senddata)
    {
        base.SentRequest(senddata, DEFINE.URL.DISTANCE);

    }

    protected override void ParsePackage(JsonData j)
    {
            IDictionary tdic = j["data"] as IDictionary;
            foreach (var t in tdic.Keys)
            {
                string ss = t.ToString();
                ServerDate date = new ServerDate(ss);
                if (!GData.g_Player.ContainSteps(date))
                {
                    m_Data.Add(date, int.Parse(j["data"][ss].ToString()));
                }
            }
            GData.g_Player.UpdateData(m_Data);
        ConnectionHandler.g_self.UnregisterConnection();
    }


}
