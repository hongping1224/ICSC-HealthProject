﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Text;
using System;
public struct ActiveData
{
    private enum ActiveType
    {
        Motionless = 0,
        Low = 1,
        Medium = 2,
        High = 3,
        Sleep = 4,
        no_watch = 5,
    }
    private static int ActiveTypeCount
    {
        get { return Enum.GetNames(typeof(ActiveType)).Length; }
    }
    public ServerDate m_Date;
    private int[] m_data;

    public ActiveData( ServerDate date,JsonData j)
    {
        m_Date = date;
        int k = ActiveTypeCount;
        m_data = new int[k];
        if (j.IsArray)
        {
            for (int i = 0; i < k; ++i)
            {
                m_data[i] = int.Parse(j[i].ToString());
            }
        }
    }
    /// <summary>
    /// 靜態強度
    /// </summary>
    public int m_Motionless
    {
        get { return m_data[(int)(ActiveType.Motionless)]; }
        set { m_data[(int)(ActiveType.Motionless)] = value; }
    }
    /// <summary>
    /// 輕度強度
    /// </summary>
    public int m_Low
    {
        get { return m_data[(int)(ActiveType.Low)]; }
        set { m_data[(int)(ActiveType.Low)] = value; }
    }
    /// <summary>
    /// 中度強度
    /// </summary>
    public int m_Medium
    {
        get { return m_data[(int)(ActiveType.Medium)]; }
        set { m_data[(int)(ActiveType.Medium)] = value; }
    }
    /// <summary>
    /// 劇烈強度
    /// </summary>
    public int m_High
    {
        get { return m_data[(int)(ActiveType.High)]; }
        set { m_data[(int)(ActiveType.High)] = value; }
    }
    /// <summary>
    /// 睡眠時間
    /// </summary>
    public int m_Sleep
    {
        get { return m_data[(int)(ActiveType.Sleep)]; }
        set { m_data[(int)(ActiveType.Sleep)] = value; }
    }
    /// <summary>
    /// 未佩戴
    /// </summary>
    public int m_NoWatch
    {
        get { return m_data[(int)(ActiveType.no_watch)]; }
        set { m_data[(int)(ActiveType.no_watch)] = value; }
    }
 
    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("date : " + m_Date + "\n");
        for (int i = 0; i < m_data.Length; i++)
        {
            sb.Append(((ActiveType)i).ToString()+" : " + m_data[i] +"\n");
        }
        return sb.ToString();
    }

    public static ActiveData empty
    {
        get { return new ActiveData(); }
    }
}
