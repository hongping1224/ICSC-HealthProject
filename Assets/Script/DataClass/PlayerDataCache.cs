﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DEFINE;
using System.Linq;
public class PlayerDataCache
{
    public User m_User = new User();
    private Dictionary<ServerDate, SleepData> m_SleepCol = new Dictionary<ServerDate, SleepData>();
    private Dictionary<ServerDate, CaloriesData> m_CaloriesCol = new Dictionary<ServerDate, CaloriesData>();
    private Dictionary<ServerDate, StepsData> m_StepsCol = new Dictionary<ServerDate, StepsData>();
    private Dictionary<ServerDate, ActiveData> m_ActiveCol = new Dictionary<ServerDate, ActiveData>();
    private Dictionary<ServerDate, int> m_DistanceCol = new Dictionary<ServerDate, int>();

    public SleepData[] GetSleepDatas()
    {
        return m_SleepCol.Values.ToArray();
    }

    public SleepData GetSleepData(ServerDate date)
    {
        if (m_SleepCol.ContainsKey(date))
        {
            return m_SleepCol[date];
        }
        else
        {
            /*SleepConnection sc = SleepConnection.Create();
            WWWForm form = RequireField.SLEEP(m_User.m_UID, date, date);
            sc.SentRequest(form);*/
            return SleepData.empty;
        }
    }
    public void UpdateData(Dictionary<ServerDate, SleepData> dic)
    {
        m_SleepCol.MergeDictionary<ServerDate, SleepData>(dic);
    }


    public CaloriesData[] GetCaloriesDatas()
    {
        return m_CaloriesCol.Values.ToArray();
    }

    public CaloriesData GetCaloriesData(ServerDate date)
    {
        if (m_CaloriesCol.ContainsKey(date))
        {
            return m_CaloriesCol[date];
        }
        else
        {
            /* SleepConnection sc = SleepConnection.Create();
             WWWForm form = RequireField.SLEEP(m_User.m_UID, date);
             sc.SentRequest(form);*/
            return CaloriesData.empty;
        }
    }
    public void UpdateData(Dictionary<ServerDate, CaloriesData> dic)
    {
        m_CaloriesCol.MergeDictionary<ServerDate, CaloriesData>(dic);
    }
    public bool ContainCalories(ServerDate key)
    {
        return m_CaloriesCol.ContainsKey(key);
    }


    public StepsData[] GetStepsDatas()
    {
        return m_StepsCol.Values.ToArray();
    }

    public StepsData GetStepsData(ServerDate date)
    {
        if (m_StepsCol.ContainsKey(date))
        {
            return m_StepsCol[date];
        }
        else
        {
            /* SleepConnection sc = SleepConnection.Create();
             WWWForm form = RequireField.SLEEP(m_User.m_UID, date);
             sc.SentRequest(form);*/
            return StepsData.empty;
        }
    }
    public void UpdateData(Dictionary<ServerDate, StepsData> dic)
    {
        m_StepsCol.MergeDictionary<ServerDate, StepsData>(dic);
    }
    public bool ContainSteps(ServerDate key)
    {
        return m_StepsCol.ContainsKey(key);
    }


    public ActiveData[] GetActiveDatas()
    {
        return m_ActiveCol.Values.ToArray();
    }

    public ActiveData GetActiveData(ServerDate date)
    {
        if (m_ActiveCol.ContainsKey(date))
        {
            return m_ActiveCol[date];
        }
        else
        {
            /*SleepConnection sc = SleepConnection.Create();
            WWWForm form = RequireField.SLEEP(m_User.m_UID, date, date);
            sc.SentRequest(form);*/
            return ActiveData.empty;
        }
    }
    public void UpdateData(Dictionary<ServerDate, ActiveData> dic)
    {
        m_ActiveCol.MergeDictionary<ServerDate, ActiveData>(dic);
    }

    public int[] GetDistanceDatas()
    {
        return m_DistanceCol.Values.ToArray();
    }

    public int GetDistanceData(ServerDate date)
    {
        if (m_DistanceCol.ContainsKey(date))
        {
            return m_DistanceCol[date];
        }
        else
        {
            return -1;
        }
    }
    public void UpdateData(Dictionary<ServerDate, int> dic)
    {
        m_DistanceCol.MergeDictionary<ServerDate, int>(dic);
    }


}
   


