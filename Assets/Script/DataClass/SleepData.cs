﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Text;
using System;
public struct SleepData
{

    private enum SleepType
    {
        sleeptime = 0,
        wakeuptime = 1,
        sleeptotaltime = 2,
        sleeplatency = 3,
        wakeupdelay = 4,
        wakeuptimes = 5,
        sleepefficiency = 6,
    }
    
    private static int SleepTypeCount
    {
        get { return Enum.GetNames(typeof(SleepType)).Length; }
    }

    public ServerDate m_Date;
    private long[] m_data;

    /// <summary>
    /// 睡眠时间(毫秒)
    /// </summary>
    public long m_SleepTime
    {
        get { return m_data[(int)(SleepType.sleeptime)]; }
        set { m_data[(int)(SleepType.sleeptime)] = value; }
    }
    /// <summary>
    /// 起床时间(毫秒)
    /// </summary>
    public long m_WakeUpTime
    {
        get { return m_data[(int)(SleepType.wakeuptime)]; }
        set { m_data[(int)(SleepType.wakeuptime)] = value; }
    }
    /// <summary>
    /// 总睡眠时间(分钟)
    /// </summary>
    public long m_SleepTotalTime
    {
        get { return m_data[(int)(SleepType.sleeptotaltime)]; }
        set { m_data[(int)(SleepType.sleeptotaltime)] = value; }
    }
    /// <summary>
    /// 睡眠延遲(分鐘)
    /// </summary>
    public long m_SleepLatency
    {
        get { return m_data[(int)(SleepType.sleeplatency)]; }
        set { m_data[(int)(SleepType.sleeplatency)] = value; }
    }
    /// <summary>
    /// 起床延遲(分鐘)
    /// </summary>
    public long m_WakeUpDelay
    {
        get { return m_data[(int)(SleepType.wakeupdelay)]; }
        set { m_data[(int)(SleepType.wakeupdelay)] = value; }
    }
    /// <summary>
    /// 睡眠翻身次數
    /// </summary>
    public long m_WakeUpTimes
    {
        get { return m_data[(int)(SleepType.wakeuptimes)]; }
        set { m_data[(int)(SleepType.wakeuptimes)] = value; }

    }
    /// <summary>
    /// 睡眠效率  % 數 
    /// max 100 %
    /// </summary>
    public long m_SleepEfficiency
    {
        get { return m_data[(int)(SleepType.sleepefficiency)]; }
        set { m_data[(int)(SleepType.sleepefficiency)] = value; }
    }

    public SleepData(ServerDate date,JsonData j)
    {
       m_Date = date;
       int k = SleepTypeCount;
        m_data = new long[k];
        for(int i=0 ; i< k ; ++i)
        {
            m_data[i] = long.Parse(j[i].ToString());
        }
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("m_Date :");
        sb.Append(m_Date);
        for (int i = 0; i < m_data.Length; i++)
        {
            sb.Append(((SleepType)i).ToString() + " : " + m_data[i] + "\n");
        }
     
        return sb.ToString();
    }
    public static SleepData empty
    {
        get { return new SleepData(); }
    }


}
