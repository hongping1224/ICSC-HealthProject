﻿using UnityEngine;
using System.Collections;
using System.IO;
public class SettingData :SaveData<SettingData>
{
    public string m_UserName;
    public string m_Password;
    protected static string m_fileLocation = Application.persistentDataPath + "/setting.xml";

    public SettingData()
    {
        m_UserName = "UserName";
        m_Password = "password";
    }

    public override void Save()
    {
        XmlManager.Save<SettingData>(this,m_fileLocation);
    }

    public new static SettingData Load()
    {
      return XmlManager.Load<SettingData>(m_fileLocation);
    }

    public new static bool ClearFile()
    {
        if (File.Exists(m_fileLocation))
        {
            File.Delete(m_fileLocation);
            return true;
        }
        return false;
    }
}

