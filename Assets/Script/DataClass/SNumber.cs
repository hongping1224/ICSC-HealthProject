﻿using UnityEngine;
using System.Collections;
using System;
public class SNumber
{
    private double m_value;
    private int pow10;

    public double Value
    {
        get
        {
            //make sure standard and rounding
            if (m_value >= 10 || m_value < 1)
                ToStandard();
            if ((Math.Ceiling(m_value) - (m_value)) % 0.0001 <= 0.00001)
            {
                m_value = Math.Ceiling(m_value * 10000) / 10000;
            }
            else if (((m_value - Math.Floor(m_value)) % 0.0001) <= 0.00001)
            {
                m_value = Math.Floor(m_value * 10000) / 10000;
            }
            return m_value;
        }
    }
    public int Pow10
    {
        get
        {
            if (m_value >= 10 || m_value < 1)
                ToStandard();
            return pow10;
        }
    }
    public SNumber()
    {
        m_value = 0;
        pow10 = 0;
    }
    public SNumber(double c, int p)
    {
        m_value = c;
        pow10 = p;
    }
    public SNumber(long xx)
    {
        m_value = xx;
        pow10 = 0;
        ToStandard();
    }
    public SNumber(double x)
    {
        m_value = x;
        pow10 = 0;
        ToStandard();

    }
    public static SNumber operator +(SNumber s1, SNumber s2)
    {
        s1.ToStandard();
        s2.ToStandard();
        while (s2.pow10 > s1.pow10)
        {
            s1.pow10++;
            s1.m_value /= 10;
        }
        while (s1.pow10 > s2.pow10)
        {
            s2.pow10++;
            s2.m_value /= 10;
        }
        SNumber s = new SNumber(s1.m_value + s2.m_value, s1.pow10);
        s.ToStandard();
        return s;
    }
    public static SNumber operator -(SNumber s1, SNumber s2)
    {
        s1.ToStandard();
        s2.ToStandard();
        while (s2.pow10 > s1.pow10)
        {
            s1.pow10++;
            s1.m_value /= 10;
        }
        while (s1.pow10 > s2.pow10)
        {
            s2.pow10++;
            s2.m_value /= 10;
        }
        SNumber s = new SNumber(s1.m_value - s2.m_value, s1.pow10);
        s.ToStandard();
        return s;
    }
    public static SNumber operator *(int s1, SNumber s2)
    {
        s2.ToStandard();
        SNumber s = new SNumber(s1 * s2.m_value, s2.pow10);
        s.ToStandard();
        return s;
    }
    public static SNumber operator *(SNumber s1, SNumber s2)
    {
        s1.ToStandard();
        s2.ToStandard();
        SNumber s = new SNumber(s1.m_value * s2.m_value, s1.pow10 + s2.pow10);
        s.ToStandard();
        return s;
    }
    public static SNumber operator /(SNumber s1, SNumber s2)
    {
        s1.ToStandard();
        s2.ToStandard();
        SNumber s = new SNumber(s1.m_value / s2.m_value, s1.pow10 - s2.pow10);
        s.ToStandard();
        return s;
    }
    public static bool operator >(SNumber s1, SNumber s2)
    {
        s1.ToStandard();
        s2.ToStandard();
        if (s1.pow10 == s2.pow10)
        {
            return s1.m_value > s2.m_value;
        }
        return s1.pow10 > s2.pow10;
    }
    public static bool operator <(SNumber s1, SNumber s2)
    {
        s1.ToStandard();
        s2.ToStandard();
        if (s1.pow10 == s2.pow10)
        {
            return s1.m_value < s2.m_value;
        }
        return s1.pow10 < s2.pow10;
    }
    public static bool operator >=(SNumber s1, SNumber s2)
    {
        s1.ToStandard();
        s2.ToStandard();
        if (s1.pow10 == s2.pow10)
        {
            return s1.m_value >= s2.m_value;
        }
        return s1.pow10 >= s2.pow10;
    }
    public static bool operator <=(SNumber s1, SNumber s2)
    {
        s1.ToStandard();
        s2.ToStandard();
        if (s1.pow10 == s2.pow10)
        {
            return s1.m_value <= s2.m_value;
        }
        return s1.pow10 <= s2.pow10;
    }
    public static bool operator ==(SNumber s1, SNumber s2)
    {
        s1.ToStandard();
        s2.ToStandard();
        if (s1.pow10 == s2.pow10)
        {
            return s1.m_value == s2.m_value;
        }
        return false;
    }

    public override bool Equals(object obj)
    {
       return (this == (SNumber)obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public static bool operator !=(SNumber s1, SNumber s2)
    {
        s1.ToStandard();
        s2.ToStandard();
        if (s1.pow10 == s2.pow10)
        {
            return s1.m_value != s2.m_value;
        }
        return true;
    }


    public void ToStandard()
    {
        while (m_value >= 10)
        {
            m_value /= 10f;
            pow10++;
        }
        while (m_value < 1 && m_value != 0)
        {
            m_value *= 10f;
            pow10--;
        }
    }
    public override string ToString()
    {
        if (Pow10 >= 6)
        {
            return Value.ToString("N4") + "e" + Pow10.ToString();
        }else if(Pow10 <0)
        {
            double v = Value;
            for(int i = Pow10 ; i!=0 ; i++)
            {
                v /= 10;
            }
            return v.ToString();
        }
        else
        {
            double v = Value;
            for (int i = 0; i < pow10; i++)
            {
                v *= 10;
            }
            return v.ToString();
        }
    }
}