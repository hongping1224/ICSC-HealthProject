﻿using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
public struct User
{
   public string m_Info;
   public long m_UID;
   public string m_Account;
   public string m_Password;
   public string m_Name;
   public bool m_Gender;
   public ServerDate m_Birthday;
   public string m_Email;
   public float m_Height;
   public float m_Weight;
   public string m_Sensor;
   public string m_Typename;

   public User(JsonData Data)
   {
       m_Info = Data["info"].ToString();
       m_UID = long.Parse(Data["u_id"].ToString());
       m_Account = Data["account"].ToString();
       m_Password = Data["password"].ToString();
       m_Name = Data["name"].ToString();
       if (Data["gender"].ToString() == "1")
       {
           m_Gender = true;
       }
       else
       {
           m_Gender = false;
       }
       m_Birthday = new ServerDate(Data["birthday"].ToString());
       m_Email = Data["email"].ToString();
       m_Height = float.Parse(Data["height"].ToString());
       m_Weight = float.Parse(Data["weight"].ToString());
       m_Sensor = Data["sensor"].ToString();
       m_Typename = Data["type_name"].ToString();
   }

   public override string ToString()
   {
       StringBuilder sb = new StringBuilder();
       sb.Append("info :");
       sb.Append(m_Info);
       sb.Append(" \n u_id :");
       sb.Append(m_UID);
       sb.Append("\n account :");
       sb.Append(m_Account);
       sb.Append("\n password :");
       sb.Append(m_Password);
       sb.Append("\n gender :");
       sb.Append(m_Gender);
       sb.Append("\n birthday :");
       sb.Append(m_Birthday);
       sb.Append("\n email :");
       sb.Append(m_Email);
       sb.Append("\n height :");
       sb.Append(m_Height);
       sb.Append("\n weight :");
       sb.Append(m_Weight);
       sb.Append("\n sensor :");
       sb.Append(m_Sensor);
       sb.Append("\n typename :");
       sb.Append(m_Typename);
       return sb.ToString();
   }

}
