﻿using UnityEngine;
using System.Collections;
using System;
using GameDataReader;
public class GameData : SaveData<GameData>
{
    private SNumber m_money;
    private SNumber m_gem;
    private uint m_level;
    private _LEVEL_ m_levelNow;
    private int m_ap;
    public const int MAXAP = 5;
    private const float AP_REGENTIME = 0.05f;
    public DateTime m_nextApRegen;
    public static readonly DateTime INFINITEDATA = new DateTime(9999, 12, 31);
    public GameData()
    {
        m_gem = new SNumber(0f, 0);
        m_money = new SNumber(0f, 0);
        m_level = 1;
        m_levelNow = _LEVEL_.GetData(m_level);
        m_ap = MAXAP;
        m_nextApRegen = INFINITEDATA;
    }

    public uint m_Level
    {
        get { return m_level; }
        protected set { m_level = value; }
    }

    public int m_AP
    {
        get
        {
            int tmpap = m_ap;
            if (tmpap < MAXAP)
            {
                DateTime now = DateTime.Now;
                if (DateTime.Equals(m_nextApRegen, INFINITEDATA))
                {
                    // go get save
                    m_nextApRegen = GData.g_date.Date;
                    m_nextApRegen = m_nextApRegen.AddMinutes(AP_REGENTIME);
                }
                DateTime tmpregen = m_nextApRegen;
                if (now >= tmpregen)
                {
                    Debug.Log("send");
                    RegenApConnection regen = new RegenApConnection();
                    regen.SentRequest();
                }
                while (now >= tmpregen)
                {
                    tmpap++;
                    tmpregen = tmpregen.AddMinutes(AP_REGENTIME);
                    if (tmpap == MAXAP)
                    {
                        tmpregen = INFINITEDATA;
                        break;
                    }
                }
            }
            return tmpap;
        }
    }
    public bool TryUseAP(int amount)
    {
        if (m_AP >= amount)
        {
            if (DateTime.Equals(m_nextApRegen, INFINITEDATA))
            {
                m_nextApRegen = DateTime.Now.AddMinutes(AP_REGENTIME);
            }
            m_ap -= amount;
            return true;
        }
        return false;
    }

    public void RefreshAp()
    {
        DateTime now = GData.g_date.Date;
        while (now >= m_nextApRegen)
        {
            Debug.Log("realregen");
            Debug.Log(m_nextApRegen);
            m_ap++;
            m_nextApRegen = m_nextApRegen.AddMinutes(AP_REGENTIME);
            if (m_ap == MAXAP)
            {
                m_nextApRegen = new DateTime(9999, 12, 31);
                break;
            }
        }
    }

    public _LEVEL_ m_LevelNow
    {
        get { return m_levelNow; }
        protected set { m_levelNow = value; }
    }

    public SNumber m_Money
    {
        get { return m_money; }
        protected set { m_money = value; }
    }

    public void GainMoney(SNumber x)
    {
        m_money += x;
        while (m_Money >= m_levelNow.LV_Require)
        {
            LevelUp();
        }
    }

    public bool TrySpendMoney(SNumber moneyUsed)
    {
        if (m_Money >= moneyUsed)
        {
            m_Money -= moneyUsed;
            return true;
        }
        return false;
    }

    public SNumber m_Gem
    {
        get { return m_gem; }
        protected set { m_gem = value; }
    }

    public bool TrySpendGem(SNumber gemUsed)
    {
        if (m_Gem >= gemUsed)
        {
            m_Gem -= gemUsed;
            return true;
        }
        return false;
    }

    public void LevelUp()
    {
        if (m_Level < _LEVEL_.m_Datas.Count)
        {
            m_Level++;
            m_LevelNow = _LEVEL_.GetData(m_Level);
        }
    }

    public void ClearMoney()
    {
        Debug.Log("clear");
        m_money = new SNumber(0, 0);
    }

}
