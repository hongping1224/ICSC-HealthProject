﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Text;

public struct CaloriesData
{
    public ServerDate m_Date;
    public int[] m_Hours;
    public int m_Total;

    public bool HaveHourData
    {
        get
        {
            return (m_Hours.Length == 24);
        }
    }


    public CaloriesData(ServerDate date, JsonData j, bool ishourdata = false)
    {
        m_Date = date;
        if (ishourdata)
        {
            m_Hours = new int[24];
            for (int i = 0; i < 24; ++i)
            {
                m_Hours[i] = int.Parse(j["hour" + (i + 1).ToString("D2")].ToString());
            }
            m_Total = int.Parse(j["total"].ToString());
        }
        else
        {
            m_Hours = new int[0];
            m_Total = int.Parse(j.ToString());
        }
    }



    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("m_Date :");
        sb.Append(m_Date);

        for (int i = 0; i < m_Hours.Length; i++)
        {
            sb.Append(" \n hour" + (i + 1) + " :");
            sb.Append(m_Hours[i]);
        }
        sb.Append("\n total :");
        sb.Append(m_Total);

        return sb.ToString();
    }

    public static CaloriesData empty
    {
        get { return new CaloriesData();}
    }

}
