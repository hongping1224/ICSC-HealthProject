﻿using UnityEngine;
using System.Collections;
using System;
public struct ServerDate
{
    private DateTime m_dateTime;

    public int Day
    {
        get { return m_dateTime.Day; }
    }

    public int Month
    {
        get { return m_dateTime.Month; }
    }
    public int Year
    {
        get { return m_dateTime.Year; }
    }

    public DateTime Date
    {
        get { return m_dateTime; }
        set { m_dateTime = value; }
    }

    public ServerDate(string date)
    {

        string[] t = new string [0];
        string[] d;
        
        if(date.Contains(":"))
        {
            t = date.Split(' ');
            date = t[0];
        }
        
        if (date.Contains("-"))
        {
            d = date.Split('-');
        }
        else
        {
            d = date.Split('/');
        }
        if(t.Length ==2)
        {
            t = t[1].Split(':');
            m_dateTime = new DateTime(int.Parse(d[0]), int.Parse(d[1]), int.Parse(d[2]),int.Parse(t[0]),int.Parse(t[1]),int.Parse(t[2]));

        }else if (d.Length >= 3)
            m_dateTime = new DateTime(int.Parse(d[0]), int.Parse(d[1]), int.Parse(d[2]));
        else
            m_dateTime = new DateTime();
    }

    public ServerDate(int year, int month, int day)
    {
        m_dateTime = new DateTime(year, month, day);
    }
    public ServerDate(DateTime t)
    {
        m_dateTime = t;
    }

    public override string ToString()
    {
        return m_dateTime.ToString("MM/dd/yyyy");
    }
}