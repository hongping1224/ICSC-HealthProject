﻿using UnityEngine;
using System.Collections;
using GameDataReader;

public static class GData
{
   public static PlayerDataCache g_Player= new PlayerDataCache();
   private static SettingData g_setting = null;
   public static GameData g_Inventory = new GameData();
   public static ServerDate g_date = new ServerDate();

   public static SettingData g_Setting
    {
        get
        {
            if (g_setting == null)
            {
                g_setting = SettingData.Load();
            }
                return g_setting;
        }
        set
        {
            g_setting = value;
        }
    }

   public static string GetCoinSpecial(ServerDate DATE)
   {
       StepsData Step = g_Player.GetStepsData(DATE);
       ActiveData Active = g_Player.GetActiveData(DATE);
       if (!Step.Equals(StepsData.empty) && !Active.Equals(ActiveData.empty))
       {
           int sum = Active.m_Low + Active.m_Medium + Active.m_High;
           int clicktimes = (Active.m_Low / sum * Step.m_Total) + (Active.m_Medium / sum * Step.m_Total) * 2 + (Active.m_High / sum * Step.m_Total) * 3;
           GData.g_Inventory.GainMoney(clicktimes * g_Inventory.m_LevelNow.Click);
           string str = "輕度:" + (Active.m_Low / sum * Step.m_Total);
           str += "中度:" + (Active.m_Medium / sum * Step.m_Total);
           str += "重度:" + (Active.m_High / sum * Step.m_Total);
           str += "總共獲得 ：" + (clicktimes * g_Inventory.m_LevelNow.Click).ToString() + "元";
           return str;
       }
       else
       {
           return "昨天沒有資料";
       }
   }

}
